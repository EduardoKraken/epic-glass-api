// app.js
require('dotenv').config(); // Cargar variables de entorno desde el archivo .env
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const fileUpload = require("express-fileupload");
const path = require('path');
const routes = require('./routes'); // Importar todas las rutas

// Inicializar la aplicación Express
const app = express();

// Middlewares
app.use(cors());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(fileUpload()); // Manejo de archivos

// Rutas estáticas para imágenes y PDFs
app.use('/fotos-epic-grass', express.static(path.join(__dirname, process.env.UPLOAD_PATH_FOTOS)));
app.use('/pdfs', express.static(path.join(__dirname, process.env.UPLOAD_PATH_PDFS)));

// Cargar rutas
app.use(routes);

module.exports = app;
