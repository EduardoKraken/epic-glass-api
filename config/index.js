// config/index.js
module.exports = {
  uploadPaths: {
    fotos: process.env.UPLOAD_PATH_FOTOS,
    pdfs: process.env.UPLOAD_PATH_PDFS
  },
  port: process.env.PORT || 3000
};
