const Almacen = require("../models/almacen.model.js");

// agregar una entrada
// exports.addAlmacen = (req, res) => {
//   if(!req.body){
//   	res.status(400).send({
//   		message:"El Contenido no puede estar vacio"
//   	});
//   }

//   Almacen.addAlmacen(req.body, (err, data)=>{
//   	if(err)
//   		res.status(500).send({
//   			message:
//   			err.message || "Se produjo algún error al crear el cliente"
//   		})
//   	else res.send(data)
//   })
// };

exports.addAlmacen = async(req, res) => {
    try {
        const getAlmacen = await Almacen.getAlmacen(req.body);

        if (getAlmacen.length > 0) {
            const updateAlmacen2 = await Almacen.updateAlmacen2(req.body);
            const almacen = await Almacen.addAlmacen(req.body);
        }

        res.send({ message: 'se agrego correctamente' });
    } catch (err) {
        res.status(500).send({
            message: err.message || "Se produjo algún error al crea el pago"
        })
    }
};

exports.getAlmacenList = (req, res) => {
    Almacen.getAlmacenList((err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los grupos"
            });
        } else {
            res.send(data)
        }
    });
};


exports.updateAlmacen = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Almacen.updateAlmacen(req.params.id, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el almacen con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el almacen con el id" + req.params.id
                });
            }
        } else res.send(data);
    });
};

exports.validarAlmacen = (req, res) => {
    // console.log(req.body)  

    let arrayArticulos = []
    for (const i in req.body) {
        arrayArticulos.push(req.body[i].id)
    }

    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    Almacen.validarAlmacen(arrayArticulos, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el cliente"
            })
        } else {
            let faltantes = []
            for (const i in req.body) {
                let almacen = data.find(articulo => articulo.id == req.body[i].id)
                if (almacen) {
                    let art = data.find(articulo => articulo.id == req.body[i].id && parseInt(req.body[i].cantidad) > parseInt(articulo.cant))
                    if (art) {
                        faltantes.push(req.body[i])
                    }
                } else {
                    console.log('si')
                    faltantes.push(req.body[i])
                }
            }

            console.log('faltantes', faltantes)
            res.send(faltantes)
        }
    })
};

exports.obtenerBodegas = async(req, res) => {
    try {
        const bodegas = await Almacen.obtenerBodegas()
        res.status(200).send(bodegas);

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las bodegas"
        })
    }
}