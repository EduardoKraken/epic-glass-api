const aromas   = require("../models/aromas.model.js");

// Enviar el recibo de pago al alumno
exports.addAromas = async(req, res) => {
  try {

    const aroma   = await aromas.addAromas( req.body ).then( response => response )

    res.send({message: 'Aroma creado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateAromas= async(req, res) => {
  try {

    const { id } = req.params

    const aroma   = await aromas.updateAromas( req.body, id ).then( response => response )

    res.send({message: 'Aroma actualizado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getAromas = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const aroma = await aromas.getAromas( ).then( response=> response ) 
      
    res.send( aroma );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getAromasActivos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const aroma = await aromas.getAromasActivos( ).then( response=> response ) 
      
    res.send( aroma );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
