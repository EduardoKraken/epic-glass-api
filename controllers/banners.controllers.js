
const Banners = require("../models/banners.model.js");
const path = require('path');
const fs = require('fs');

// Agregar un laboratorio
// exports.addBanner = (req, res) => {
// 	// Validacion de request
//   if(!req.body){
//   	res.status(400).send({
//   		message:"El Contenido no puede estar vacio"
//   	});
//   }

//   // Guardar el CLiente en la BD
//   Banners.addBanner(req.body, (err, data)=>{
//   	// EVALUO QUE NO EXISTA UN ERROR
//   	if(err)
//   		res.status(500).send({
//   			message:
//   			err.message || "Se produjo algún error al crear el Laboratorio"
//   		})
//   	else res.send(data)

//   })
// };

exports.addBanner = async (req, res) => {
  try {
    // Validación del request
    if (!req.body || !req.files) {
      return res.status(400).send({
        message: "El Contenido no puede estar vacío o falta el archivo",
      });
    }
    // Extraer el nombre de las imágenes
    const bannerPC = req.files.banner_pc;
    const bannerMobil = req.files.banner_mobil;

    // Guardar el nombre de la imagen en la base de datos
    const bannerResult = await Banners.addBanner(bannerPC.name, bannerMobil.name);

    // Definir rutas absolutas donde se guardarán las imágenes (subir tres niveles)
    const bannerPcPath = path.join(__dirname, '../../../fotos-epic-grass/', bannerPC.name);
    const bannerMobilPath = path.join(__dirname, '../../../fotos-epic-grass/', bannerMobil.name);
    // Mover las imágenes al directorio designado
    await bannerPC.mv(bannerPcPath);
    await bannerMobil.mv(bannerMobilPath);
    
    // Si ambos procesos son exitosos, enviamos la respuesta
    return res.status(200).send({
      message: "Banner guardado y archivo subido correctamente",
      data: bannerResult,
    });
    
  } catch (error) {
    // Manejo de errores
    return res.status(500).send({
      message: error.message || "Se produjo algún error al crear el Banner o subir el archivo",
    });
  }
};



// Traer laboratorios por id
exports.getBanners = (req, res)=>{
    Banners.getBanners((err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

const eliminarArchivo = (filePath) => {
  return new Promise((resolve, reject) => {
    fs.access(filePath, fs.constants.F_OK, (err) => {
      if (err) {
        // Si el archivo no existe, resolver sin error
        return resolve();
      }
      // Si el archivo existe, proceder a eliminarlo
      fs.unlink(filePath, (err) => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });
  });
};

// Delete a users with the specified usersId in the request
exports.eliminar_banner = async (req, res) => {
  try{

    // Validación del request
    if (!req.body) {
      return res.status(400).send({
        message: "El Contenido no puede estar vacío o falta el archivo",
      });
    }

    const bannersId = await Banners.bannerPorId(req.body.idbanners);

    if (!bannersId || bannersId.length === 0) {
      return res.status(404).send({ message: "El banner no existe en la base de datos" });
    }

    const banner = bannersId[0];
    const bannerPcPath = path.join(__dirname, '../../../fotos-epic-grass/', banner.nombanner);
    const bannerMobilPath = path.join(__dirname, '../../../fotos-epic-grass/', banner.banner_mobil);

    // Eliminar el registro de la base de datos
    const bannersEliminar = await Banners.eliminar_banner(banner.idbanners);
    // Usar la función separada para eliminar las imágenes físicamente
    await eliminarArchivo(bannerPcPath);
    await eliminarArchivo(bannerMobilPath);

    return res.status(200).send({  message: "Banner eliminado correctamente" });
  }catch(error){
    // Manejo de errores
    return res.status(500).send({
      message: error.message || "Se produjo algún error al eliminar el banner",
    });
  }

};



// Banners.deleteBanner(req.params.idbanners, (err, data) => {
//   if (err) {
//     if (err.kind === "not_found") {
//       res.status(404).send({
//         message: `Not found Users with id ${req.params.idbanners}.`
//       });
//     } else {
//       res.status(500).send({
//         message: "No encontre el usuario con el id " + req.params.idbanners
//       });
//     }
//   } else res.send({ message: `El usuario se elimino correctamente!` });
// });