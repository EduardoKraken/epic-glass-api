
const Clientes = require("../models/clientes.model.js");
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const privateKey = process.env.STR_PRIVATE_RSA_KEY;
// exports.session_cliente = (req, res)=>{
//   Clientes.login_cliente(req.body,(err, data)=>{
//     if (err) {
//       if (err.kind === "not_found") {
//         res.status(404).send({
//           message: `Este cliente no se encuentra registrado`
//         });
//       } else {
//         res.status(500).send({
//           message: "Error al buscar al cliente" 
//         });
//       }
//     } else {
// 			console.log('data',data)
// 			if(data){
// 				res.status(200).send(data);
// 			}else{
// 				res.status(404).send({message:'ESTE USUARIO NO SE ENCUENTRA REGISTRADO'});
// 			}
// 		}
//   });
// };

exports.session_cliente = async (req, res) => {
  try {
      
      const { nombre, password } = req.body; // Obtener el nombre de usuario y contraseña desde el body
      const password_salt = await Clientes.obtenerPasswordSalt(nombre); // Consultamos cifrado de bits para password de usuario. 
      console.log('nombre', nombre);
      
      if (password_salt.salt == null) {
          return res.status(404).send({ message: "Usuario no encontrado." });
      }

      let password_hash = await generarPasswordHash(password.trim(), password_salt.salt)
      const cliente = await Clientes.login_cliente({ nombre, password: password_hash }); // Verificar en la base de datos que el usuario existe y las credenciales son correctas
      console.log("🚀 ~ cliente:", cliente)
      
      if (!cliente) {
          return res.status(404).send({  message: `Usuario no encontrado o contraseña incorrecta.` });
      }
      
      //* Obtenemos token para servicio de login sys
      let clienteToken = await getAuthTokenCliente(cliente.idcliente, cliente.usuario);
      // Devolver el token generado y el usuario
      res.status(200).send({
          message: 'Inicio de sesión exitoso',
          access_token: clienteToken.token,
          usuario: {
              id: cliente.idcliente,
              usuario: cliente.usuario,
          }
      });
  } catch (error) {
      console.error('Error durante el login:', error);
      res.status(500).send({
          message: "Error al iniciar sesión",
          error: error.message
      });
  }
};

const generarPasswordHash = async(password,salt) => {
  return new Promise((resolve, reject) => {
      try{
          let hashedPassword = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');// Crear hash de la contraseña combinada con el salt

           resolve(hashedPassword) ; 
      }catch(error){
          reject(`Ocurrió un error al generar Hash ${error.message}`)
      }
  });
};

const getAuthTokenCliente = (idcliente, usuario) => {
  return new Promise((resolve, reject) => {
      try{
          const token = jwt.sign(
              {
                _id: idcliente,
                _usuario: usuario,
              },
               privateKey.replace(/\\n/g, "\n"),
              { algorithm: "RS256", expiresIn: process.env.INT_TIEMPO_EXPIRACION }
            );
            
          Clientes.guardarTokenCliente(idcliente, token ).then( response =>{
              resolve(response) ; 
          }).catch(error =>{
          reject(error.message)
          })
      }catch(error){
          reject(`Ocurrió un error al almacenar el Token de Acceso ${error.message}`)
      }
     
  });
};

exports.registrar_nuevo_cliente = async(req, res) => {
  try{
    if(!req.body){
        res.status(404).send({ message: "No hay información para registrar cliente." });
    }

    const existencia = await Clientes.validar_existencia_cliente(req.body);

    if(!existencia.length){
            
        let salt = crypto.randomBytes(16).toString('hex'); // Generar codigo salt 
        let hashedPassword = await generarPasswordHash(req.body.password, salt)
        // Asignar el hash y el salt al objeto req.body
        req.body.password = hashedPassword;
        req.body.salt = salt;

        const nCliente  = await Clientes.registrar_nuevo_cliente(req.body);
        res.status(200).send({ message: "Cliente creado correctamente" });

    }else{
        res.status(404).send({ message: "Esté usuario ya se encuentra registrado, favor de revisar."})
    }

  }catch(error){
    res.status(500).send({
        message: error.message || "Se produjo algún error al registrar cliente."
    });
  }
	// // Validacion de request
  // if(!req.body){
  // 	res.status(400).send({
  // 		message:"El Contenido no puede estar vacio"
  // 	});
  // }

  // // Guardar el CLiente en la BD
  // Clientes.addCliente(req.body, (err, data)=>{
  // 	// EVALUO QUE NO EXISTA UN ERROR
  // 	if(err)
  // 		res.status(500).send({
  // 			message:
  // 			err.message || "Se produjo algún error al crear el cliente"
  // 		})
  // 	else res.status(200).send({ message:'El cliente se creo correctamente'})
  // })
};

exports.updateCliente = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Clientes.updateCliente(req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el cliente con el id ${req.body.data.idcliente }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el cliente con el id" + req.body.data.idcliente 
				});
			}
		} 
		else res.status(200).send({ message: 'La información se actualizo correctamente.'});
	}
	);
}

exports.cambioContraseniaCliente = async(req, res) => {
  try {
      if (!req.body) {
          res.status(500).send({ message: "Los parametros son requeridos" });
      } else {
          // req.body.password = md5(req.body.password)
          const password_salt = await Clientes.obtenerPasswordSalt(req.body.idcliente);

          if(password_salt.salt == null){
              return res.status(500).send({ message: "La información del usuario es incorrecta." });
          }
           // Asegúrate de que el salt y la contraseña estén correctos antes de continuar
          let hashedPassword = await generarPasswordHash(req.body.password.trim(),password_salt.salt)

          req.body.password = hashedPassword;
          const password = await Clientes.cambioContraseniaCliente(req.body)
          res.status(200).send({ message: "Se cambio la contraseña correctamente." });
      }

  } catch (error) {
      res.status(500).send({
          message: error.message || "Se produjo algún error al consultar el tipo de usuario"
      })
  }
};




exports.getClienteId = (req, res)=>{
    Clientes.getClienteId(req.body,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.getClientes = (req,res) => {
    Clientes.getClientes((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los clientes"
        });
      else res.send(data);
    });
};



exports.OlvideContraCliente = (req, res) => {
  Clientes.OlvideContraCliente(req.body,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(202).send({
          message: `No encontre el correo`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el correo" 
        });
      }
    } else res.send(data);
  });
};

exports.passwordExtraCliente = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  Clientes.passwordExtraCliente(req.body,(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(400).send({
            message: `No encontre el cliente con el id ${req.body.id }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el cliente con el id" + req.body.id 
          });
        }
      } 
      else res.send(data);
    }
  );
};







