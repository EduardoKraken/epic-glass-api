const combos   = require("../models/combos.model.js");

exports.obtener_presentaciones = async(req, res) => {
  try {
    const presentacion = await combos.obtener_presentaciones();
    res.send(presentacion);
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.obtener_unidades = async(req, res) => {
  try {
    const unidad = await combos.obtener_unidades();
    res.send(unidad);
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.obtener_marcas = async(req, res) => {
  try {
    const marca = await combos.obtener_marcas();
    res.send(marca);
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};


exports.obtener_olores = async(req, res) => {
  try {
    const olor = await combos.obtener_olores( );
    res.send(olor);
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_productos_bodega_almacen = async(req, res) => {
  try {
    
    if(!req.params.idbodega){
      return res.status(404).send({message: 'No se logro encontra parametro de busqueda'})
    }
    
    const productos = await combos.obtener_productos_bodega_almacen(req.params.idbodega);
    res.send(productos);
  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};




