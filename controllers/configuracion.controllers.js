const Configuracion = require("../models/configuracion.model.js");

// Traer laboratorios 
exports.getConfiguracion = (req, res) => {
    Configuracion.getConfiguracion((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los grupos"
            });
        else res.send(data[0]);
    });
};

// Actualizar un laboratorio
exports.updateConfig = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Configuracion.updateConfig(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el ciaws con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el ciaws con el id" + req.params.id
                });
            }
        } else res.send(data);
    });
};


exports.costoEnvio = async(req, res) => {
    try {
        const cCostoEnvio = await Configuracion.costoEnvio();
        console.log('cCostoEnvio', cCostoEnvio);
        res.status(200).send(cCostoEnvio);
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
};

exports.actualizaCostoEnvio = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const uCostoEnvio = await Configuracion.actualizaCostoEnvio(req.body);
            res.status(200).send({ message: "El costo de envio se actualizo correctamente." });
        }
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al actualizar costo de envio"
        })
    }
};