
const Cupones = require("../models/cupones.model.js");


exports.getCupones = (req, res)=>{
  Cupones.getCupones((err,data)=>{
    if(err)
      res.status(500).send({message:err.message || "Se produjo algún erro al recuperar los cupones."});
    else res.send(data);
  });
};

// **********
exports.validar_cupon_existente = async (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }
  const cupon = await Cupones
    .validar_cupon_existente(req.body).then((response) => response)
    .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));
    
    if(!cupon.length){
      res.status(400).send({ message: "Esté cupon no se encuentra disponible" });
    }else{
      res.status(200).send(cupon);
    }

};

// Crear un cliente
exports.addCupon = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Cupones.addCupon(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cupon"
      })
    else res.send(data)
  })
};



exports.updateCupon = (req, res) =>{
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }
  
  Cupones.updateCupon(req.params.id, req.body ,(err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({message: `No encontre el cupon con el id ${ req.params.id } `});
      } else {
        res.status(500).send({message: `Error al actualizar el cupon con el id ${ req.params.id }`});
      }
    } 
    else res.send(data);
  }
  );
}
