// Guardar imagenes
exports.addImagen = (req, res) => {
    console.log('req imagenes', req);
    let EDFile = req.files.file
    EDFile.mv(`./../../fotos-epic-grass/${EDFile.name}`, err => {
        if (err) return res.status(500).send({ message: err })

        return res.status(200).send({ message: 'File upload' })
    })
};


// Guardar PDF

exports.addPdf = (req, res) => {
    let EDFile = req.files.file
    EDFile.mv(`./../../pdf/${EDFile.name}`, err => {
        if (err) return res.status(500).send({ message: err })
        return res.status(200).send({ message: 'File upload' })
    })
};

exports.getDocumento = (req, res) => {
    var file = './../../pdf/' + req.params.nom;
    res.download(file); // Set disposition and send it.
}