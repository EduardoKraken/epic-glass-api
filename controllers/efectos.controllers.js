const efectos   = require("../models/efectos.model.js");

// Enviar el recibo de pago al alumno
exports.addEfectos = async(req, res) => {
  try {

    const efecto   = await efectos.addEfectos( req.body ).then( response => response )

    res.send({ message: `El efecto se creo correctamente` });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateEfectos= async(req, res) => {
  try {

    const { id } = req.params

    const efecto   = await efectos.updateEfectos( req.body, id ).then( response => response )

    res.send({ message: `El efecto se actualizo correctamente` });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getEfectoss = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const efecto = await efectos.getEfectoss( ).then( response=> response ) 
      
    res.send( efecto );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getEfectosActivos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const efecto = await efectos.getEfectosActivos( ).then( response=> response ) 
      
    res.send( efecto );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
