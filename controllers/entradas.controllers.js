const Entradas = require("../models/entradas.model.js");
const Almacen = require("../models/almacen.model.js");
const cAlmacen = require('../controllers/almacen.controllers') // --> ADDED THIS

// ***************************************************************************************
// *                            FUNCIONES PROBADAS                                       * 
// ***************************************************************************************
// CONSULTAR TODAS LAS ENTRADAS | ADMIN |
exports.getEntradasAll = async(req, res) => {
    try {
        const cEntradas = await Entradas.getEntradasAll(req.body);
        res.status(200).send(cEntradas);

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las entradas."
        });
    }
};

// traer las entradas por fecha
exports.getEntradasFecha = async(req, res) => {

    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const cEntradas = await Entradas.getEntradasFecha(req.body);
            res.status(200).send(cEntradas);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las Entradas."
        });
    }
};

exports.addEntradas = async (req, res) => {
    try{
        if (!req.body) {
            res.status(400).send({ message: "El Contenido no puede estar vacio"  });
        }

        const mEntradas = await Entradas.addEntradas(req.body);
        const getAlmacen = await Almacen.getAlmacen(req.body);
        
        if (getAlmacen.length > 0) {
            await Almacen.updateAlmacen2(req.body.cant ,getAlmacen[0].idalmacen);
        }else{
            await Almacen.addAlmacen(req.body);
        }

        res.status(200).send({ message: "La entrada se creo correctamente."})

    }catch(error){
        res.status(500).send({
            message: error.message || "Se produjo algún error al dar de alta la entrada."
        }); 
    }
};
// ******************************************


exports.updateEntrada = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Entradas.updateEntrada(req.params.id, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el almacen con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el almacen con el id" + req.params.id
                });
            }
        } else res.send(data);
    });
};