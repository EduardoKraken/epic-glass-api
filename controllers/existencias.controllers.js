const Existencias = require("../models/existencias.model.js");

exports.getExistencias =  async(req ,res) => {
  try{

    if (req.params.idbodegas == null || req.params.idbodegas == undefined) {
        res.status(400).send({ message: "El Contenido no puede estar vacio"  });
    }

    const idbodegas = parseInt(req.params.idbodegas, 10);
    const existencias = await Existencias.getExistencias(idbodegas);
    res.status(200).send(existencias);

  }catch(error){
    res.status(500).send({
        message: error.message || "Se produjo algún error al consultar las existencias."
    }); 
  }
};


// agregar una entrada
exports.getExistenciaCodigo = (req, res) => {
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Existencias.getExistenciaCodigo(req.params.codigo, (err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

// agregar una entrada
exports.getExistenciaCodigoDes = (req, res) => {
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  Existencias.getExistenciaCodigoDes(req.params.codigo, (err, data)=>{
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear el cliente"
      })
    else res.send(data)
  })
};


// // traer las entradas por fecha
exports.getExistenciaTotal = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  Existencias.getExistenciaTotal((err, data)=>{
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el cliente"
  		})
  	else res.send(data)
  })
};

