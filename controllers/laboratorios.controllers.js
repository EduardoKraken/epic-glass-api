
const Laboratorio = require("../models/laboratorios.model.js");
const { v4: uuidv4 } = require('uuid')

/***************************************************************************************************/
/*                                        CATEGORÍA                                                */
/***************************************************************************************************/
exports.getCategorias = (req,res) => {
  Laboratorio.getCategorias((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

exports.addFotoCategoria = async (req, res) => {
  try{
    if( !req.files ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }

    let EDFile = req.files.file
    EDFile.mv(`./../../fotos-epic-grass/${EDFile.name}`, err => {
        if (err) return res.status(500).send({ message: err })
        return res.status(200).send({ message: 'Imagen cargada correctamente', nombre: EDFile.name })
    })
  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error al guardar imagen de categoría' } )
  }
};

exports.addCategorias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addCategorias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Categoría"
  		})
      else res.send({message:'Categoría creada correctamente.'});

  })
};

exports.updateCategorias = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateCategorias(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Categoría Actualizada correctamente.'});
	}
	);
}

exports.eliminarCategoria = (req, res) =>{
	if (!req.params.id) {
		res.status(400).send({ message: "No se obtuvo información para eliminar categoría"});
	}
	Laboratorio.eliminarCategoria(req.params.id,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la categoría con el identificador ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al eliminar la categoría con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Categoría Eliminada correctamente.'});
	}
	);
}

exports.getCategoriasActivo = (req,res) => {
  Laboratorio.getCategoriasActivo((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

/***************************************************************************************************/
/*                                           FAMILIA                                               */
/***************************************************************************************************/
exports.getFamilias = (req,res) => {
  Laboratorio.getFamilias((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar las familias"
      });
    else res.send(data);
  });
};

exports.addFamilias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addFamilias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear una nueva familia"
  		})
      else res.send({message:'Familia Creada correctamente.'});
  })
};

exports.updateFamilias = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateFamilias(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la familia con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar la familia con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Familia Actualizada correctamente.'});
	}
	);
}

exports.eliminarFamilia = (req, res) =>{
	if (!req.params.id) {
		res.status(400).send({ message: "No se obtuvo información para eliminar familia"});
	}
	Laboratorio.eliminarFamilia(req.params.id,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la familia con el identificador ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al eliminar la familia con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Familia Eliminada correctamente.'});
	}
	);
}

exports.getFamiliasActivo = (req,res) => {
  Laboratorio.getFamiliasActivo((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

/***************************************************************************************************/
/*                                     SUBCATEGORIAS                                               */
/***************************************************************************************************/

exports.eliminarSubCategoria = (req, res) =>{
	if (!req.params.id) {
		res.status(400).send({ message: "No se obtuvo información para eliminar subcategoria"});
	}
	Laboratorio.eliminarSubCategoria(req.params.id,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre la subcategoria con el identificador ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al eliminar la subcategoria con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Subcategoria Eliminada correctamente.'});
	}
	);
}

exports.getSubcategorias = (req,res) => {
  Laboratorio.getSubcategorias((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los grupos"
      });
    else res.send(data);
  });
};

exports.updateSubCatego = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Laboratorio.updateSubCatego(req.params.id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el laboratorio con el id ${req.params.id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el laboratorio con el id" + req.params.id 
				});
			}
		} 
		else res.send({message:'Subcategoria actualizada correctamente.'});
	}
	);
}

exports.addSubcategorias = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Laboratorio.addSubcategorias(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Laboratorio"
  		})
      else res.send({message:'Subcategoria creada correctamente.'});

  })
};


// >>>>>>>>> SECCION NO VERIFICADA >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>> 
// >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 

exports.getCategoriaPorFamilia = (req,res) => {
    Laboratorio.getCategoriaPorFamilia(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};



exports.addCategoriasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addCategoriasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getCategoriasArtId = (req,res) => {
    Laboratorio.getCategoriasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deleteCategoriasArt = (req, res) => {
  Laboratorio.deleteCategoriasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};


exports.getCatActivos = (req,res) => {
    Laboratorio.getCatActivos((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.getSubcategoriasActivo = (req,res) => {
    Laboratorio.getSubcategoriasActivo((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};







exports.addsubCategoriasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addsubCategoriasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getsubCategoriasArtId = (req,res) => {
    Laboratorio.getsubCategoriasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deletesubCategoriasArt = (req, res) => {
  Laboratorio.deletesubCategoriasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};

/***********************************************************************************************/
// Traer laboratorios por id
exports.getSubcategoriasPorCatego = (req, res)=>{
    Laboratorio.getSubcategoriasPorCatego(req.params.idcatego,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};




exports.addFamiliasArt = (req, res) => {
  // Validacion de request
  if(!req.body){
    res.status(400).send({
      message:"El Contenido no puede estar vacio"
    });
  }

  // Guardar el CLiente en la BD
  Laboratorio.addFamiliasArt(req.body, (err, data)=>{
    // EVALUO QUE NO EXISTA UN ERROR
    if(err)
      res.status(500).send({
        message:
        err.message || "Se produjo algún error al crear la familia del artículo"
      })
    else res.send(data)

  })
};

exports.getFamiliasArtId = (req,res) => {
    Laboratorio.getFamiliasArtId(req.params.id,(err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.deleteFamiliasArt = (req, res) => {
  Laboratorio.deleteFamiliasArt(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.id
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};



/***************************************************************************************************/
/*                                           TIENDA                                                */
/***************************************************************************************************/

exports.getMenuTienda = async (req, res) => {
  try {
    const familias = await Laboratorio.obtenerFamilias();
    const categorias = await Laboratorio.obtenerCategorias();
    const subcategorias = await Laboratorio.obtenerSubcategorias();

    const menu = familias.map(familia => ({
      idfamilias: familia.idfamilias,
      familia: familia.familia,
      categorias: categorias.filter(categoria => categoria.idfamilias === familia.idfamilias).map(categoria => ({
          idcategorias: categoria.idcategorias,
          categoria: categoria.categoria,
          subcategorias: subcategorias.filter(subcategoria => subcategoria.idcatego === categoria.idcategorias).map(subcategoria => ({
              idsubcategoria: subcategoria.idsubcategorias,
              subcategoria: subcategoria.subcategoria
            }))
        }))
    }));
    // Enviar el menú como respuesta
    res.send(menu);
  } catch (err) {
    // Manejo de errores
    res.status(500).send({
      message: err.message || "Se produjo algún error al recuperar los grupos"
    });
  }
};

