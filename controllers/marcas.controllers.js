const marcas   = require("../models/marcas.model.js");

// Enviar el recibo de pago al alumno
exports.agregar_marca = async(req, res) => {
  try {

    const marca   = await marcas.agregar_marca( req.body ).then( response => response )
    res.send({message: 'La marca se creo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.actualiza_marca= async(req, res) => {
  try {

    const { id } = req.params
    const marca   = await marcas.actualiza_marca( req.body, id ).then( response => response )
    res.send({message: 'La marca se actualizo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_marcas_catalogo = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const marca = await marcas.obtener_marcas_catalogo()
    res.send(marca);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_marcas_activos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const marca = await marcas.obtener_marcas_activos( ).then( response=> response ) 
      
    res.send( marca );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
