const olores   = require("../models/olores.model.js");

// Enviar el recibo de pago al alumno
exports.agregar_olor = async(req, res) => {
  try {

    const olor   = await olores.agregar_olor( req.body ).then( response => response )
    res.send({message: 'La olor se creo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.actualiza_olor= async(req, res) => {
  try {

    const { id } = req.params
    const olor   = await olores.actualiza_olor( req.body, id ).then( response => response )
    res.send({message: 'La olor se actualizo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_olores_catalogo = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const olor = await olores.obtener_olores_catalogo()
    res.send(olor);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_olores_activos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const olor = await olores.obtener_olores_activos().then( response=> response ) 
      
    res.send( olor );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
