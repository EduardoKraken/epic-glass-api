const Ordenes = require("../models/ordenes.model.js");
const Documentos = require("./documentos.controllers.js");


// agregar una entrada
exports.addOrden = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    Ordenes.addOrden(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la orden"
            })
        else res.send(data)
    })
};

// traer las entradas por fecha
exports.getOrdenesUsuario = (req, res) => {
    // Validacion de request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    Ordenes.getOrdenesUsuario(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el cliente"
            })
        else res.send(data)
    })
};

exports.getOrdenes = async(req, res) => {
    try {
        // Paso #1 Obtener los datos de docum
        const pedidos = await Ordenes.getDocumen(req.body);
        // console.log("🚀 ~ pedidos:", pedidos)
        const movimientosPromises = pedidos.map(pedido => Ordenes.getMovimientos(pedido.iddocum));
        // console.log("🚀 ~ movimientosPromises:", movimientosPromises)
        const movimientos = await Promise.all(movimientosPromises);
        // console.log("🚀 ~ movimientos:", movimientos)

        const direcciones = await Ordenes.getDirecciones();
        const clientes = await Ordenes.getClientesOrden();
        const pagosMercadoPago = await Ordenes.getPagosMercaddoPago();

        const pedidosRespuesta = pedidos.map((pedido, index) => {
            
            const docum = {
                ...pedido,
                movim: movimientos[index],
                direcciones: direcciones.filter(dir => dir.idpago == pedido.idpago),
                cliente: clientes.find(cli => cli.idcliente == pedido.idcliente),
                pagado: pagosMercadoPago.filter(pago => pago.preference_id == pedido.idpago)
            };
            return docum;
        });
        const resultado = pedidosRespuesta.filter(pedido => pedido.pagado.length > 0);
        res.send(resultado);

    } catch (e) {
        res.status(500).send({ message: e || "Se produjo algún error al crea el pago" })
    }
};

exports.getOrdenesCliente = async(req, res) => {
    try {
        // Paso #1 Obtener los datos de docum
        const pedidos = await Ordenes.getDocumenCli(req.params.idcliente)
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

        let pedidosRespuesta = []

        // Paso 2-. Obtener de cada uno de ellos los movimientos
        const movimientos = await Ordenes.getMovimientos()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));


        // Paso #3-. construir el arreglo de movimientos

        for (const i in pedidos) {
            let docum = {
                iddocum: pedidos[i].iddocum,
                idcliente: pedidos[i].idcliente,
                iddireccionfacturacion: pedidos[i].iddireccionfacturacion,
                iddirecion: pedidos[i].iddirecion,
                idpago: pedidos[i].idpago,
                descuento: pedidos[i].descuento,
                subtotal: pedidos[i].subtotal,
                total: pedidos[i].total,
                idcupon: pedidos[i].idcupon,
                envio: pedidos[i].envio,
                envio_gratis: pedidos[i].envio_gratis,
                descuento_cupon: pedidos[i].descuento_cupon,
                movim: [],
                direcciones: null,
                facturacion: null,
                cliente: null,
                rastreo: null
            }

            pedidosRespuesta.push(docum)
        }

        // Paso #5 consultamos las direcciones
        const direcciones = await Ordenes.getDirecciones()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

        // Paso #6 consultamos las direcciones de facturación
        const direccionesFac = await Ordenes.getDireccionesFac()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

        // Paso #7 consultamos al cliente
        const clientes = await Ordenes.getClientesOrden()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

        // Paso #8 consultamos los rastreos
        const rastreos = await Ordenes.getRastreoCliente()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));

        // Paso #8 cargar los pagos
        const pagos_mercadopago = await Ordenes.getPagosMercaddoPago()
            .then((response) => response)
            .catch((err) => res.status(500).send({ message: err.message || "Se produjo un error." }));


        for (const i in pedidosRespuesta) {
            const movimiento = movimientos.filter((movim) => {
                var doc = movim.iddocum;
                return doc == pedidosRespuesta[i].iddocum;
            })
            pedidosRespuesta[i].movim = movimiento
        }

        // Agregar la direccion
        for (const i in pedidosRespuesta) {
            const direccion = direcciones.filter(dir => dir.idpago == pedidosRespuesta[i].idpago)
            pedidosRespuesta[i].direcciones = direccion
        }

        // Agregar la factura
        for (const i in pedidosRespuesta) {
            const factura = direccionesFac.filter(dir => dir.idpago == pedidosRespuesta[i].idpago)
            pedidosRespuesta[i].facturacion = factura
        }

        // Agregar al cliente
        for (const i in pedidosRespuesta) {
            const cliente = clientes.filter(cli => cli.idcliente == pedidosRespuesta[i].idcliente)
            pedidosRespuesta[i].cliente = cliente
        }

        // Agregar el rastreo
        for (const i in pedidosRespuesta) {
            const rastreo = rastreos.filter(ras => ras.idpago == pedidosRespuesta[i].idpago)
            pedidosRespuesta[i].rastreo = rastreo
        }

        // Agregar al cliente
        for (const i in pedidosRespuesta) {
            const pago = pagos_mercadopago.filter(pago => pago.preference_id == pedidosRespuesta[i].idpago)
            pedidosRespuesta[i].pagado = pago
        }

        let resultado = []

        for (const i in pedidosRespuesta) {
            if (pedidosRespuesta[i].pagado.length > 0) {
                resultado.push(pedidosRespuesta[i])
            }
        }

        res.send(resultado);
    } catch (e) {
        res.status(500).send({ message: e || "Se produjo algún error al crea el pago" })
    }
};

exports.getOrdenId = (req, res) => {
    Ordenes.getOrdenId(req.params.id, (err, docum) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar las ordenes"
            });
        } else {
            Ordenes.getMovim(req.params.id, (err, movim) => {
                if (err) {
                    res.status(500).send({
                        message: err.message || "Se produjo algún error al recuperar las ordenes"
                    });
                } else {
                    console.log('docum', docum)
                    var respuesta = {
                        iddocum: docum[0].iddocum,
                        idusuariosweb: docum[0].idusuariosweb,
                        direccion: docum[0].direccion,
                        importe: docum[0].importe,
                        descuento: docum[0].descuento,
                        subtotal: docum[0].subtotal,
                        total: docum[0].total,
                        iva: docum[0].iva,
                        folio: docum[0].folio,
                        estatus: docum[0].estatus,
                        nota: docum[0].nota,
                        divisa: docum[0].divisa,
                        hora: docum[0].hora,
                        fecha: docum[0].fecha,
                        fechapago: docum[0].fechapago,
                        refer: docum[0].refer,
                        tipodoc: docum[0].tipodoc,
                        movim: movim
                    }

                    console.log('respuest', respuesta)

                    res.send(respuesta)
                }
            })
        }
    });
};

exports.updateOrden = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Ordenes.updateOrden(req.params.id, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el almacen con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el almacen con el id" + req.params.id
                });
            }
        } else res.send(data);
    });
};

exports.updateOrdenEnvio = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Ordenes.updateOrdenEnvio(req.params.id, req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el almacen con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el almacen con el id" + req.params.id
                });
            }
        } else res.send(data);
    });
};

exports.asignarMaquiladorPedido = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const iPedidos = await Ordenes.asignarMaquiladorPedido(req.body);
            // Mando el estatus 3 = Por entregar
            const uPedido = await Ordenes.actualizarPedido(3, req.body.idpedidos);
            res.status(200).send({ message: "Asignacion Procesada Correctamente" });
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
}

exports.obtenerMaquilacionesUsuario = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            console.log('req.body', req.body);
            
            const cPedidos = await Ordenes.obtenerPedidosxMaquilador(req.body);
            console.log("🚀~ cPedidos:", cPedidos)
            let pedidosRespuesta = []

            for (const i in cPedidos) {
                let cArticulos = await Ordenes.obtenerProductosxPedido(cPedidos[i].idpedidos);
                console.log("🚀 ~ cArticulos:", cArticulos)
                const payload = {
                    ...cPedidos[i],
                    tiempo: '00:00',
                    articulos: cArticulos
                }
                pedidosRespuesta.push(payload);
            }

            res.status(200).send(pedidosRespuesta);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
}

exports.actualizarProcesoPedido = async(req, res) => {
    try {
        console.log('LLEGO');
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            let updateFiles = '';
            let obsUpdate = '';
            switch (req.body.proceso) {
                case 2:
                    updateFiles = 'comienzo_maquilacion';
                    obsUpdate = 'obs_comienzo_maquilacion'
                    break;
                case 3:
                    updateFiles = 'termina_maquilacion';
                    obsUpdate = 'obs_termina_maquilacion'
                    break;
                case 4:
                    updateFiles = 'asignacion_repartidor';
                    obsUpdate = 'obs_asignacion_repartidor'
                    break;
                case 5:
                    updateFiles = 'por_entregar';
                    obsUpdate = 'obs_por_entregar'
                    break;
                case 6:
                    updateFiles = 'entregado';
                    obsUpdate = 'obs_entregado'
                    break;
            }
            const uPedidos = await Ordenes.actualizarProcesoPedido(req.body, updateFiles, obsUpdate);
            res.status(200).send({ message: "Asignacion Procesada Correctamente" });
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
}

exports.asignarRepartidorPedido = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const uPedidos = await Ordenes.asignarRepartidorPedido(req.body);
            res.status(200).send({ message: "Asignacion Procesada Correctamente" });
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
};

exports.PedidosPendientes = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const cPedidos = await Ordenes.obtenerPedidosPendiente(req.body);
            // console.log('cPedidos', cPedidos);
            let pedidosRespuesta = []

            for (const i in cPedidos) {
                let cArticulos = await Ordenes.obtenerProductosxPedido(cPedidos[i].idpedidos);
                const payload = {
                    ...cPedidos[i],
                    articulos: cArticulos
                }
                pedidosRespuesta.push(payload);
            }
            // console.log('pedidosRespuesta', pedidosRespuesta);
            res.status(200).send(pedidosRespuesta);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al asignar pedido a maquilador"
        })
    }
}

exports.finalizarEntrega = async(req, res) => {
    try {
        const { image_name, idseguimiento_pedidos } = req.body;
        if (image_name != '') {
            await Ordenes.guardarRegistroImagen(idseguimiento_pedidos, image_name);
        }
        await Ordenes.actualizarProcesoPedido(req.body, 'entregado', 'obs_entregado');
        await Ordenes.actualizarPedido(5, req.body.idpedidos); // 5 ES EL ESTATUS DE ENTREGADA PARA EL PEDIDO GENERAL
        await Ordenes.cobroEnvioRepartidor(req.body.idpedidos, req.body.envio);

        res.status(200).send({ message: 'Pedido Finalizado Correctamente' });
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al finalizar el pedido."
        });
    }
};


exports.historialPedido = async(req, res) => {
    try {
        const cPedido = await Ordenes.historialPedido(req.body.idpedidos);
        const cEvidencia = await Ordenes.evidenciasEntregas(cPedido[0].idseguimiento_pedidos);
        cPedido[0].imagenes = cEvidencia;

        res.status(200).send(cPedido);

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar Historial del pedido."
        });
    }
}

exports.retenerPedido = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            await Ordenes.retenerPedido(req.body);
            await Ordenes.actualizarPedido(4, req.body.idpedidos) // 4 ES EL ESTATUS DE RETENIDA DEL PEDIDO EN GENERAL
            res.status(200).send({ message: "Pedido retenido correctamente." });
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al retner el pedido."
        });
    }
};