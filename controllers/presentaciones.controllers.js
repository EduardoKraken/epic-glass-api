const presentaciones   = require("../models/presentaciones.model.js");

// Enviar el recibo de pago al alumno
exports.agregar_presentacion = async(req, res) => {
  try {

    const presentacion   = await presentaciones.agregar_presentacion( req.body ).then( response => response )

    res.send({message: 'La presentación se creo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.actualiza_presentacion= async(req, res) => {
  try {

    const { id } = req.params
    console.log('id', id);
    
    const presentacion   = await presentaciones.actualiza_presentacion( req.body, id ).then( response => response )

    res.send({message: 'La presentación se actualizo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_presentacion_catalogo = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const presentacion = await presentaciones.obtener_presentacion_catalogo()
    console.log('presentacion', presentacion);
    
    res.send(presentacion);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_presentaciones_activos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const presentacion = await presentaciones.obtener_presentaciones_activos( ).then( response=> response ) 
      
    res.send( presentacion );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
