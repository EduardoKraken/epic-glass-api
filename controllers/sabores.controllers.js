const sabores   = require("../models/sabores.model.js");

// Enviar el recibo de pago al alumno
exports.addSabores = async(req, res) => {
  try {

    const categoria   = await sabores.addSabores( req.body ).then( response => response )

    res.send({message: 'El Sabor se creo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateSabores= async(req, res) => {
  try {

    const { id } = req.params

    const categoria   = await sabores.updateSabores( req.body, id ).then( response => response )

    res.send({message: 'El Sabor se actualizo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getSabores = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const categoria = await sabores.getSabores( ).then( response=> response ) 
      
    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getSaboresActivos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const categoria = await sabores.getSaboresActivos( ).then( response=> response ) 
      
    res.send( categoria );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
