const Salidas = require("../models/salidas.model.js");
const Almacen = require("../models/almacen.model.js");

// traer las entradas por fecha
exports.getSalidasFecha = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const cSalidas = await Salidas.getSalidasFecha(req.body);
            res.status(200).send(cSalidas);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las Entradas."
        });
    }


};

exports.addSalidas = async(req, res) => {
    try{
        if (!req.body) {
            res.status(400).send({
                message: "El Contenido no puede estar vacio"
            });
        }

        const producto = await Almacen.getAlmacen(req.body);
        
        if(producto.length > 0){
            if( producto[0].cant >= req.body.cant ){

                const salida  = await Salidas.addSalidas(req.body);
                const almacen = await Almacen.updateAlmacenSalida(req.body.cant , producto[0].idalmacen);
                res.status(200).send({ message: "Salida registrada correctamente" });  

            }else{
                res.status(400).send({
                    message: "La cantidad en inventario es menor a la que requiere darle salida. Por favor revise."
                });
            }
        }else{
            res.status(404).send({
                message: "No se encontro inventario de esté producto en el almacén."
            });
        }

    }catch(error){
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las entradas."
        });
    }
};


exports.getSalidasAll = async(req, res) => {
    try {
        const cSalidas = await Salidas.getSalidasAll();
        res.status(200).send(cSalidas);
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar las entradas."
        });
    }
};