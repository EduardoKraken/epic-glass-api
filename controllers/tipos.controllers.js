const tipos   = require("../models/tipos.model.js");

// Enviar el recibo de pago al alumno
exports.addTipos = async(req, res) => {
  try {

    const tipo   = await tipos.addTipos( req.body ).then( response => response )

    res.send({message: 'Tipo se ha creado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.updateTipos= async(req, res) => {
  try {

    const { id } = req.params

    const tipo   = await tipos.updateTipos( req.body, id ).then( response => response )

    res.send({message: 'Tipo se ha actualizado correctamente' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getTipos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const tipo = await tipos.getTipos( ).then( response=> response ) 
      
    res.send( tipo );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.getTiposActivos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const tipo = await tipos.getTiposActivos( ).then( response=> response ) 
      
    res.send( tipo );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
