const unidades   = require("../models/unidades.model.js");

// Enviar el recibo de pago al alumno
exports.agregar_unidad = async(req, res) => {
  try {

    const unidad   = await unidades.agregar_unidad( req.body ).then( response => response )
    res.send({message: 'La unidad se creo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.actualiza_unidad= async(req, res) => {
  try {

    const { id } = req.params
    const unidad   = await unidades.actualiza_unidad( req.body, id ).then( response => response )
    res.send({message: 'La unidad se actualizo correctamente.' });

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_unidades_catalogo = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const unidad = await unidades.obtener_unidades_catalogo()
    res.send(unidad);

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};

exports.obtener_unidades_activos = async(req, res) => {
  try {

    // REQUISICIÓN DE COMPRA
    const unidad = await unidades.obtener_unidades_activos( ).then( response=> response ) 
      
    res.send( unidad );

  } catch (error) {
    res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
  }
};
