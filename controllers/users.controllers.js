const Users = require("../models/users.model.js");
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

// Obtener las claves RSA desde el archivo .env
const privateKey = process.env.STR_PRIVATE_RSA_KEY;
const publicKey = process.env.STR_PUBLIC_KEY;

exports.session = async (req, res) => {
    try {
        
        const { email, password } = req.body; // Obtener el nombre de usuario y contraseña desde el body
        const password_salt = await Users.obtenerPasswordSalt(email); // Consultamos cifrado de bits para password de usuario. 
        
        if (password_salt.salt == null) {
            return res.status(404).send({ message: "Usuario no encontrado." });
        }

        let password_hash = await generarPasswordHash(password.trim(), password_salt.salt)
        const user = await Users.login({ email, password: password_hash }); // Verificar en la base de datos que el usuario existe y las credenciales son correctas
        
        
        if (!user) {
            return res.status(404).send({  message: `Usuario no encontrado o contraseña incorrecta.` });
        }
        
        //* Obtenemos token para servicio de login sys
        let usuarioToken = await getAuthToken(user.idusuariosweb,user.email);
        // Devolver el token generado y el usuario
        res.status(200).send({
            message: 'Inicio de sesión exitoso',
            access_token: usuarioToken.token,
            usuario: {
                id: user.idusuariosweb,
                nomuser: user.nomuser,
                email: user.email,
                idniveles: user.idniveles,
                idbodegas: user.idbodegas
            }
        });
    } catch (error) {
        console.error('Error durante el login:', error);
        res.status(500).send({
            message: "Error al iniciar sesión",
            error: error.message
        });
    }
};

const getAuthToken = (idusuariosweb, email) => {
    return new Promise((resolve, reject) => {
        try{
            const token = jwt.sign(
                {
                  _id: idusuariosweb,
                  _usuario: email,
                },
                 privateKey.replace(/\\n/g, "\n"),
                { algorithm: "RS256", expiresIn: process.env.INT_TIEMPO_EXPIRACION }
              );
              
            Users.guardarToken(idusuariosweb, token ).then( response =>{
                resolve(response) ; 
            }).catch(error =>{
            reject(error.message)
            })
        }catch(error){
            reject(`Ocurrió un error al almacenar el Token de Acceso ${error.message}`)
        }
       
    });
};

// MODULO DE CATALOGO DE USUARIOS
exports.obtener_usuarios_catalogo = async(req, res) => {
    try {
        const cUsuarios = await Users.obtener_usuarios_catalogo();
        res.status(200).send(cUsuarios);
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar usuarios."
        });
    }
};

// MODULO DE CATALOGO DE USUARIOS
exports.agregar_nuevo_usuario_catalogo = async(req, res) => {
    try {
        if(!req.body){
            res.status(404).send({ message: "No hay información para agregar un usuario." });
        }

        //Validar que no se encuentre registrado el usuario.
        const existencia = await Users.validar_existencia_usuario(req.body);
        
        if(!existencia.length){
            
            let salt = crypto.randomBytes(16).toString('hex'); // Generar codigo salt 
            let hashedPassword = await generarPasswordHash(req.body.password, salt)
            // Asignar el hash y el salt al objeto req.body
            req.body.password = hashedPassword;
            req.body.salt = salt;

            const nUsuarios  = await Users.agregar_nuevo_usuario_catalogo(req.body);
            res.status(200).send({ message: "Usuario creado correctamente" });

        }else{
            res.status(404).send({ message: "Esté usuario ya se encuentra registrado, favor de revisar."})
        }
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al agregar el usuario."
        });
    }
};

const generarPasswordHash = async(password,salt) => {
    return new Promise((resolve, reject) => {
        try{
            let hashedPassword = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');// Crear hash de la contraseña combinada con el salt

             resolve(hashedPassword) ; 
        }catch(error){
            reject(`Ocurrió un error al generar Hash ${error.message}`)
        }
    });
};

// PROCESO PARA ACTUALIZAR PERFIL
exports.actualizar_usuario_catalogo = async (req, res) => {
    try {
        if(!req.body){
            res.status(404).send({ message: "No hay información para agregar un usuario." });
        }

        //Validar que no se encuentre registrado el usuario.
        const existencia = await Users.validar_modificacion_usuario(req.params.idusuariosweb, req.body);
        
        if(!existencia.length){
            // Transformar password a constraseña cifrada
            const nUsuarios  = await Users.actualizar_usuario_catalogo(req.params.idusuariosweb, req.body);
            res.status(200).send({ message: "Usuario actualizado correctamente" });
        }else{
            res.status(404).send({ message: "Esté usuario ya se encuentra registrado, favor de revisar."})
        }
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al agregar el usuario."
        });
    }
};

exports.eliminar_usuario_catalogo= async(req, res) => {
    try {
  
      const { idusuariosweb } = req.params
      const usuario   = await Users.eliminar_usuario_catalogo(idusuariosweb);
      res.send({message: 'El usuario se elimino correctamente.' });
  
    } catch (error) {
      res.status(500).send({ message: error ? error.message : 'Error en el servidor' })
    }
};

exports.cambioContrasenia = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            // req.body.password = md5(req.body.password)
            const password_salt = await Users.obtenerPasswordSalt(req.body.idusuario);

            if(password_salt.salt == null){
                return res.status(500).send({ message: "La información del usuario es incorrecta." });
            }
             // Asegúrate de que el salt y la contraseña estén correctos antes de continuar
            let hashedPassword = await generarPasswordHash(req.body.password.trim(),password_salt.salt)

            req.body.password = hashedPassword;
            const password = await Users.cambioContrasenia(req.body)
            res.status(200).send({ message: "Se cambio la contraseña correctamente." });
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar el tipo de usuario"
        })
    }
};

// ********************************************************************************
// ************ FUNCIONES NO PROBADAS *********************************************
// ********************************************************************************


// Crear y salvar un nuevo usuario
exports.create = (req, res) => {

    if (!req.body) { // Validacion de request
        res.status(400).send({
            message: "El Contenido no puede estar vacio"
        });
    }

    // const user = new User({ // Crear un usuario
    // 	email     : req.body.email,
    //   nomuser   : req.body.nomuser,
    // 	nomper    : req.body.nomper,
    //   nivel     : req.body.nivel,
    // 	password  : req.body.password,
    //   estatus   : req.body.estatus
    // })

    Users.create(req.body, (err, data) => { // Guardar el CLiente en la BD

        if (err) // EVALUO QUE NO EXISTA UN ERROR
            res.status(500).send({
            message: err.message || "Se produjo algún error al crear el Usuario"
        })
        else res.send(data)

    })
};

exports.findAll = (req, res) => { // Retrieve all userss from the database.
    Users.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            });
        else res.send(data);
    });
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
    Users.findById(req.params.userId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el cliente con el id ${ req.params.userId }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al recuperar el usuario con el id" + req.params.userId
                });
            }
        } else res.send(data);
    });
};

// Find a single users with a usersId
exports.getxEmail = (req, res) => {
    Users.getxEmail(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(202).send({
                    message: `No encontre el usuario`
                });
            } else {
                res.status(500).send({
                    message: "Error al recuperar el usuario"
                });
            }
        } else res.send(data);
    });
};

// Update a users identified by the usersId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Users.updateById(req.params.userId, new User(req.body), (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(400).send({
                    message: `No encontre el usuario con el id ${req.params.userId }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el usuario con el id" + req.params.userId
                });
            }
        } else res.send(data);
    });
};

// Delete a users with the specified usersId in the request
exports.delete = (req, res) => {
    Users.remove(req.params.usersId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Users with id ${req.params.usersId}.`
                });
            } else {
                res.status(500).send({
                    message: "No encontre el usuario con el id " + req.params.usersId
                });
            }
        } else res.send({ message: `El usuario se elimino correctamente!` });
    });
};

// Delete all userss from the database.
exports.deleteAll = (req, res) => {
    Users.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al eliminar a todos los clientes."
            });
        else res.send({ message: `Todos los usuarios se eliminarion correctamente!` });
    });
};

exports.activarUsuario = (req, res) => {
    Users.activarUsuario(req.params.idusuario, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No encontre el usuario con el id ${req.params.idusuario }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el usuario con el id" + req.params.idusuario
                });
            }
        } else res.send(data);
    });
};

exports.OlvideContra = (req, res) => {
    Users.OlvideContra(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(202).send({
                    message: `No encontre el correo`
                });
            } else {
                res.status(500).send({
                    message: "Error al recuperar el correo"
                });
            }
        } else res.send(data);
    });
};

exports.passwordExtra = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }
    Users.passwordExtra(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(400).send({
                    message: `No encontre el usuario con el id ${req.body.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al actualizar el usuario con el id" + req.body.id
                });
            }
        } else res.send(data);
    });
};

exports.obtenerTipoUsuario = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "Los parametros son requeridos" });
        } else {
            const usuarios = await Users.obtenerTipoUsuario(req.body)
            res.status(200).send(usuarios);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar el tipo de usuario"
        })
    }
};

exports.obtenerNiveles = async(req, res) => {
    try {
        const cNiveles = await Users.obtenerNiveles();
        res.status(200).send(cNiveles);
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al retner el pedido."
        });
    }
};

exports.usuariosAdmin = async(req, res) => {
    try {
        const cUsuarios = await Users.usuariosAdmin();
        res.status(200).send(cUsuarios);
    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar usuarios administradores."
        });
    }
};


