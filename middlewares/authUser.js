const jwt = require('jsonwebtoken');

const authUser = async (req, res, next) => {
    try {
        const authHeader = req.header('Authorization');

        if (!authHeader) {
            res.status(401).send({
                success: false,
                error: 98,
                msg: 'No se recibió token',
                magDetail: ''
            })
            return;
        }

        const token = authHeader.replace('Bearer ', '').trim();

        if (token.trim() == '') {
            res.status(401).send({
                success: false,
                error: 98,
                msg: 'Token vacío',
                magDetail: ''
            });
            return;
        }

        const publicKey = process.env.STR_PUBLIC_KEY.replace(/\\n/g, '\n');
        const data = jwt.verify(token, publicKey, { algorithms: ['RS256'] });
        // console.log("🚀 ~ authUser ~ data:", data)
        req.usuario = data;
        req.usuario.token = token;

        next();
    } catch (error) {
        console.log('errr', error);
        
        res.status(401).send({
            success: false,
            error: 99,
            msg: 'El token se encuentra vencido. Inicie sesión nuevamente',
            msgDetail: ''
        });
    }
}

module.exports = authUser;