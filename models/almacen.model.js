const sql = require("../config/db.js");

// constructor

const Alamacen = almacen => {
    this.codigo = almacen.codigo;
    this.lote = almacen.lote;
    this.cant = almacen.cant;
    this.caducidad = almacen.caducidad;
};

Alamacen.addAlmacen = (ent, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO almacen (idbodegas, idproductos, codigo,cant)
                    VALUES(?,?,?,?)`, [ent.idbodegas, ent.idproductos, ent.codigo, ent.cant], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

Alamacen.getAlmacen = (art, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM almacen WHERE idproductos = ? AND idbodegas = ?`, [art.idproductos, art.idbodegas], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

Alamacen.updateAlmacen2 = (cant, idalmacen, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE almacen SET cant = cant + ? WHERE idalmacen =?`, [cant, idalmacen], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

Alamacen.updateAlmacenSalida = (cant, idalmacen, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE almacen SET cant = cant - ? WHERE idalmacen =?`, [cant, idalmacen], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

//* FUNCIONES NO PROBADAS ***************************************************************
// **************************************************************************************

// Alamacen.addAlmacen = (ent, result) => {
//   sql.query(`INSERT INTO almacen (codigo,lote,cant,caducidad)
//                                   VALUES(?,?,?,?)`,
//     [ent.codigo,ent.lote,ent.cant,ent.caducidad], (err, res) => {  
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//       return;
//     }
//     // console.log("Crear Grupo: ", res.insertId);
//     console.log("Crear almacen: ", { id: res.insertId, ...ent });
//     result(null, { id: res.insertId, ...ent });
//   });
// };

Alamacen.getAlmacenList = result => {
    sql.query(`SELECT a.*, ar.nomart FROM almacen a 
    LEFT JOIN arts ar ON ar.codigo = a.codigo;`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("almacen: ", res);
        result(null, res);
    });
};

Alamacen.updateAlmacen = (id, alm, result) => {
    sql.query(`UPDATE almacen SET cant = ? WHERE idalmacen = ? `, [alm.cant, id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("updated almacen: ", { id: id, ...alm });
        result(null, { id: id, ...alm });
    });
};

Alamacen.validarAlmacen = (ent, result) => {
    sql.query(`SELECT a.codigo, a.id, SUM(alm.cant) AS cant , a.nomart, 
            (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto 
    FROM almacen alm LEFT JOIN arts a ON a.codigo = alm.codigo WHERE a.id IN (?) GROUP BY a.codigo, a.id;`, [ent], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("almacen: ", res);
        result(null, res);
    });
};

Alamacen.obtenerBodegas = (p, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM bodegas`, (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

module.exports = Alamacen;