const sql = require("../config/db.js");

//const constructor
const aromas = (clases) => {};

aromas.addAromas = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO aromas( aroma, estatus ) VALUES( ?, ? )`, [ u.aroma, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

aromas.updateAromas = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE aromas SET aroma = ?, estatus = ?, deleted = ?  WHERE idaromas = ?`,[ u.aroma, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

aromas.getAromas = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM aromas WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


aromas.getAromasActivos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM aromas WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = aromas;