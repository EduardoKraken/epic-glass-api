const sql = require("../config/db.js");

const Articulos = articulos => {
    this.id             = articulos.id;
    this.nomart         = articulos.nomart;
    this.codigo         = articulos.codigo;
    this.idlaboratorio  = articulos.idlaboratorio;
    this.nomlab         = articulos.nomlab;
    this.descrip        = articulos.descrip;
    this.statusweb      = articulos.statusweb;
    this.precio1        = articulos.precio1;
    this.sal            = articulos.sal;
    this.novedades      = articulos.novedades;
    this.destacados     = articulos.destacados;
    this.pjedesc        = articulos.pjedesc;
    this.alto           = articulos.alto;   
    this.largo          = articulos.largo;
    this.ancho          = articulos.ancho;
    this.peso           = articulos.peso;
}

Articulos.obtener_articulos_random = result => {
  sql.query(`SELECT 
              a.id, 
              a.nomart, 
              a.codigo, 
              a.precio1, 
              a.precio2, 
              a.pjedesc, 
              c.largo, 
              c.ancho, 
              c.alto, 
              c.peso, 
              'SIN ENVOLTORIO' AS envoltorio,
                (SELECT image_name 
                  FROM fotos AS f
                  WHERE 
                      f.idproducto = a.id 
                  AND f.principal = 1 
                  ORDER BY f.principal 
                  DESC 
                  LIMIT 1
                ) AS foto
              FROM arts AS a 
                LEFT JOIN caracteristicas AS c ON c.id_articulo = a.id
              WHERE a.estatus = 1 
              ORDER BY RAND() LIMIT 12`, (err, res) => {
                  if (err) { result(null, err);  return; }
    console.log("ARTICULOS RANDOM: ", res);
    result(null, res);
  });
};

Articulos.obtener_articulos = result => {
  sql.query(`SELECT 
              a.id, 
              a.nomart, 
              a.codigo, 
              a.precio1, 
              a.precio2, 
              a.pjedesc, 
              c.largo, 
              c.ancho, 
              c.alto, 
              c.peso,
              'SIN ENVOLTORIO' AS envoltorio,
                (SELECT image_name 
                  FROM fotos AS f
                  WHERE 
                      f.idproducto = a.id
                  AND f.principal = 1 
                  ORDER BY f.principal 
                  DESC 
                  LIMIT 1
                ) AS foto
              FROM arts AS a 
                LEFT JOIN caracteristicas AS c ON c.id_articulo = a.id
              WHERE a.estatus = 1`, (err, res) => {
                  if (err) { result(null, err);  return; }
    console.log("ARTICULOS: ", res);
    result(null, res);
  });
};

Articulos.getAll = result => {
  sql.query(`SELECT 
              art.id
              ,art.nomart 
              ,art.codigo
              ,art.precio1
              ,art.precio2
              ,art.pjedesc
              ,efe.efecto
              ,car.id_articulo
              ,sab.sabor
              ,aro.aroma
              ,tip.tipo
              ,olo.olor
              ,pre.presentacion
              ,mar.marca
              ,env.envoltorio
              ,uni.unidad
              ,car.cantidad
              ,car.thc
              ,car.cbd
              ,car.cbg
              ,car.energia
              ,car.descripcion
              ,car.descripcion_larga
              ,car.largo
              ,car.ancho
              ,car.alto
              ,car.peso
              ,f.familia
              ,IFNULL(c.categoria,'NA')    AS categoria
              ,IFNULL(s.subcategoria,'NA') AS subcategoria
            FROM epic_glass.caracteristicas AS car
              LEFT JOIN arts 		 	      AS art ON car.id_articulo 		= art.id	
              LEFT JOIN efectos 		    AS efe ON car.id_efecto 		  = efe.idefectos 
              LEFT JOIN sabores    	    AS sab ON car.id_sabor 			  = sab.idsabores
              LEFT JOIN aromas 		      AS aro ON car.id_aroma 			  = aro.idaromas
              LEFT JOIN tipos			      AS tip ON car.id_tipo   		  = tip.idtipos
              LEFT JOIN olores			    AS olo ON car.id_olor       	= olo.idolores
              LEFT JOIN presentaciones  AS pre ON car.id_presentacion = pre.idpresentaciones
              LEFT JOIN marcas			    AS mar ON car.id_marca 			  = mar.idmarca
              LEFT JOIN envoltorios	    AS env ON car.id_envoltorio   = env.idenvoltorios
              LEFT JOIN unidades		    AS uni ON car.id_unidad			  = uni.idunidades
              LEFT JOIN categorias      AS c   ON c.idcategorias 		  = art.idcategorias
              LEFT JOIN familias        AS f   ON f.idfamilias 			  = art.idfamilias
              LEFT JOIN subcategorias   AS s   ON s.idsubcategorias 	= art.idsubcategorias
            `, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getArticulosActivo = result => {
  sql.query(`SELECT 
              art.id
              ,art.nomart 
              ,art.codigo
              ,art.precio1
              ,art.precio2
              ,art.pjedesc
              ,efe.efecto
              ,car.id_articulo
              ,sab.sabor
              ,aro.aroma
              ,tip.tipo
              ,olo.olor
              ,pre.presentacion
              ,mar.marca
              ,env.envoltorio
              ,uni.unidad
              ,car.cantidad
              ,car.thc
              ,car.cbd
              ,car.cbg
              ,car.energia
              ,car.descripcion
              ,car.descripcion_larga
              ,car.largo
              ,car.ancho
              ,car.alto
              ,car.peso
              ,f.familia
              ,IFNULL(c.categoria,'NA')    AS categoria
              ,IFNULL(s.subcategoria,'NA') AS subcategoria
            FROM epic_glass.caracteristicas AS car
              LEFT JOIN arts 		 	      AS art ON car.id_articulo 		= art.id	
              LEFT JOIN efectos 		    AS efe ON car.id_efecto 		  = efe.idefectos 
              LEFT JOIN sabores    	    AS sab ON car.id_sabor 			  = sab.idsabores
              LEFT JOIN aromas 		      AS aro ON car.id_aroma 			  = aro.idaromas
              LEFT JOIN tipos			      AS tip ON car.id_tipo   		  = tip.idtipos
              LEFT JOIN olores			    AS olo ON car.id_olor       	= olo.idolores
              LEFT JOIN presentaciones  AS pre ON car.id_presentacion = pre.idpresentaciones
              LEFT JOIN marcas			    AS mar ON car.id_marca 			  = mar.idmarca
              LEFT JOIN envoltorios	    AS env ON car.id_envoltorio   = env.idenvoltorios
              LEFT JOIN unidades		    AS uni ON car.id_unidad			  = uni.idunidades
              LEFT JOIN categorias      AS c   ON c.idcategorias 		  = art.idcategorias
              LEFT JOIN familias        AS f   ON f.idfamilias 			  = art.idfamilias
              LEFT JOIN subcategorias   AS s   ON s.idsubcategorias 	= art.idsubcategorias
            WHERE 
                car.es_eliminado = 0 
            AND car.es_activo = 1
            AND art.estatus = 1
            `, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.getArticuloId = (id,result) => {
  sql.query(`SELECT 
              art.id
              ,art.nomart 
              ,art.codigo
              ,art.precio1
              ,art.precio2
              ,art.pjedesc
              ,efe.idefectos
              ,efe.efecto
              ,car.id_articulo
              ,sab.idsabores
              ,sab.sabor
              ,aro.idaromas
              ,aro.aroma
              ,tip.idtipos
              ,tip.tipo
              ,olo.idolores
              ,olo.olor
              ,pre.idpresentaciones
              ,pre.presentacion
              ,mar.idmarca
              ,mar.marca
              ,env.idenvoltorios
              ,env.envoltorio
              ,uni.idunidades
              ,uni.unidad
              ,car.cantidad
              ,car.thc
              ,car.cbd
              ,car.cbg
              ,car.energia
              ,car.descripcion
              ,car.descripcion_larga
              ,car.largo
              ,car.ancho
              ,car.alto
              ,car.peso
              ,f.idfamilias
              ,f.familia
              ,IFNULL(c.categoria,'NA')    AS categoria
              ,IFNULL(s.subcategoria,'NA') AS subcategoria
            FROM epic_glass.caracteristicas AS car
              LEFT JOIN arts 		 	      AS art ON car.id_articulo 		= art.id	
              LEFT JOIN efectos 		    AS efe ON car.id_efecto 		  = efe.idefectos 
              LEFT JOIN sabores    	    AS sab ON car.id_sabor 			  = sab.idsabores
              LEFT JOIN aromas 		      AS aro ON car.id_aroma 			  = aro.idaromas
              LEFT JOIN tipos			      AS tip ON car.id_tipo   		  = tip.idtipos
              LEFT JOIN olores			    AS olo ON car.id_olor       	= olo.idolores
              LEFT JOIN presentaciones  AS pre ON car.id_presentacion = pre.idpresentaciones
              LEFT JOIN marcas			    AS mar ON car.id_marca 			  = mar.idmarca
              LEFT JOIN envoltorios	    AS env ON car.id_envoltorio   = env.idenvoltorios
              LEFT JOIN unidades		    AS uni ON car.id_unidad			  = uni.idunidades
              LEFT JOIN categorias      AS c   ON c.idcategorias 		  = art.idcategorias
              LEFT JOIN familias        AS f   ON f.idfamilias 			  = art.idfamilias
              LEFT JOIN subcategorias   AS s   ON s.idsubcategorias 	= art.idsubcategorias
            WHERE 
                car.es_eliminado = 0 
            AND car.es_activo = 1
            AND art.id = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res[0]);
    result(null, res[0]);
  });
};

// *Se usa en : Editor de articulos 
Articulos.updateArticulos = (id, art, result) => {
  return new Promise((resolve, reject) => {
      sql.query(` UPDATE arts 
              SET nomart = ?, 
                  idfamilias = ?, 
                  idcategorias = ?, 
                  idsubcategorias =?, 
                  estatus = ?, 
                  precio1 = ?, 
                  pjedesc = ? 
                WHERE id = ?;`, 
    [ art.nomart
      ,art.idfamilias
      ,art.idcategorias 
      ,art.idsubcategorias
      ,art.estatus
      ,art.precio1
      ,art.pjedesc
      ,id
    ],(err, res) => {
          if (err) {
              reject(err)
              return;
          }
          resolve(res)
      });
  })
};

// *Se usa en : Editor de articulos 
Articulos.actualizaCaracteristicas = (id, art, result) => {
  return new Promise((resolve, reject) => {
      sql.query(` UPDATE caracteristicas 
                  SET id_efecto = ?, 
                      id_sabor = ?, 
                      id_aroma = ?, 
                      id_tipo =?, 
                      id_presentacion = ?,
                      id_unidad = ?,
                      id_marca = ?,
                      id_olor = ?,
                      descripcion = ?, 
                      descripcion_larga = ?, 
                      largo = ?,
                      ancho = ?,
                      alto = ?,
                      peso = ?,
                      thc = ?,
                      cbd = ?,
                      cbg = ?,
                      energia = ?
                    WHERE id_articulo = ?;`, 
    [ art.idefectos
      ,art.idsabores
      ,art.idaromas 
      ,art.idtipos
      ,art.idpresentaciones
      ,art.idunidades
      ,art.idmarca
      ,art.idolores
      ,art.descripcion
      ,art.descripcion_larga
      ,art.largo
      ,art.ancho
      ,art.alto 
      ,art.peso
      ,art.thc
      ,art.cbd
      ,art.cbg
      ,art.energia
      ,id
    ],(err, res) => {
          if (err) {
              reject(err)
              return;
          }
          resolve(res)
      });
  })
};
 //* Se usa en: Editor de articulos.
Articulos.addFoto = (foto, result) => {
  sql.query(`INSERT INTO fotos(idproducto, codigo, image_name, principal)VALUES(?,?,?,?)`,
    [foto.idproducto, foto.codigo,foto.image_name,foto.principal], (err, res) => {  
    if (err) {  
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Almacenar foto de producto: ", { id: res.insertId, ...foto });
    result(null, { id: res.insertId, ...foto });
  });
};

Articulos.obtener_novedades = result => {
  sql.query(`SELECT  
              a.id, 
              a.nomart, 
              a.codigo, 
              a.precio1, 
              a.precio2, 
              a.pjedesc, 
              c.largo, 
              c.ancho, 
              c.alto, 
              c.peso,
              'SIN ENVOLTORIO' AS envoltorio,
                (SELECT image_name 
                  FROM fotos AS f
                  WHERE 
                      f.idproducto = a.id
                  AND f.principal = 1 
                  ORDER BY f.principal 
                  DESC 
                  LIMIT 1
                ) AS foto
              FROM arts AS a 
                LEFT JOIN caracteristicas AS c ON c.id_articulo = a.id 
            WHERE 
                a.estatus =1 
            `, (err, res) => {
              // AND a.novedades = 1
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("NOVEDADES: ", res);
    result(null, res);
  });
};




/* PENDIENTE DE REVISAR USO -- NO EXISTE TABLA LABORATORIOS ******************************************************************
*****************************************************************************************************************************/
Articulos.getNovedades = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc 
              FROM arts a 
                LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio 
              WHERE a.novedades = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

/* PENDIENTE DE REVISAR USO -- NO EXISTE TABLA LABORATORIOS ******************************************************************
*****************************************************************************************************************************/
Articulos.getDestacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.novedades, 
    a.destacados, a.pjedesc FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.destacados = 1 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

/* PENDIENTE DE REVISAR USO -- NO EXISTE CAMPO DE NOVEDADES ******************************************************************
*****************************************************************************************************************************/


/* PENDIENTE DE REVISAR USO -- NO EXISTE CAMPO DE DESTACADOS *****************************************************************
*****************************************************************************************************************************/
Articulos.obtener_destacados = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, a.largo, a.ancho, a.alto, a.peso, a.envoltorio,
              a.destacados, a.descrip, a.estatus,
                  (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto
              FROM arts a WHERE a.estatus =1 AND a.destacados = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("DESTACADO: ", res);
    result(null, res);
  });
};

/* PENDIENTE DE REVISAR USO -- NO EXISTE SECCION DE JUGUETES *****************************************************************
*****************************************************************************************************************************/
Articulos.obtener_juguetes = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.precio1, a.precio2, a.pjedesc, 
                a.juguete, a.descrip, a.estatus,
                  (SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1) AS foto
              FROM arts a WHERE a.estatus =1 AND a.juguete = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("JUGUETES: ", res);
    result(null, res);
  });
};

/* PENDIENTE DE REVISAR USO -- NO EXISTE TABLA LABORATORIOS ******************************************************************
*****************************************************************************************************************************/
Articulos.getArtxLab =(id, result) => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio 
    WHERE  a.statusweb = 1 AND a.idlaboratorio = ?;`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

/* PENDIENTE DE REVISAR USO -- NO EXISTE TABLA LABORATORIOS ******************************************************************
*****************************************************************************************************************************/
Articulos.getPromociones = result => {
  sql.query(`SELECT a.id, a.nomart, a.codigo, a.sal, a.idlaboratorio, l.nomlab, a.descrip, a.statusweb, a.precio1, a.pjedesc
    FROM arts a LEFT JOIN laboratorios l ON l.idlaboratorios = a.idlaboratorio WHERE a.pjedesc > 0 AND a.statusweb = 1;`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};


Articulos.fotosArt = result => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};

Articulos.fotosxArt = (codigo, result) => {
  sql.query(`SELECT idfotos, codigo, image_name, principal FROM fotos WHERE codigo = ?`,[codigo], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("articulos: ", res);
    result(null, res);
  });
};



Articulos.addArticulos = (art, result) => {
  sql.query(`INSERT INTO arts(nomart,codigo,idfamilias,idcategorias,idsubcategorias,descrip,descripLarga)VALUES(?,?,?,?,?,?,?)`,
    [art.nomart,art.codigo,art.idfamilias,art.idcategorias,art.idsubcategorias,art.descrip,art.descripLarga], (err, res) => {  
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Agregar Articulo: ", { id: res.insertId, ...art });
    result(null, { id: res.insertId, ...art });
  });
};


Articulos.getArticuloCodigo = (codigo, result) => {
  sql.query(`SELECT a.*, f.familia, c.categoria,s.subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias
    WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    console.log("found students: ", res);
    result(null, res);
  });
};


Articulos.getArticuloTienda = (codigo, result) => {
  sql.query(`SELECT a.*, f.familia, c.categoria,s.subcategoria FROM arts a 
    LEFT JOIN categorias c ON c.idcategorias = a.idcategorias
    LEFT JOIN familias f ON f.idfamilias = a.idfamilias
    LEFT JOIN subcategorias s ON s.idsubcategorias = a.idsubcategorias
    WHERE a.codigo = ? OR a.nomart like '%` + codigo +`%';`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    console.log("found students: ", res);
    result(null, res);
  });
};

Articulos.updateNovedades = (id, art, result) => {
  sql.query(` UPDATE arts SET novedades = ?, destacados = ?, juguete  = ? WHERE id = ?;`, [art.novedades,art.destacados, art.juguete, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated articulos: ", { id: id, ...art });
      result(null, { id: id, ...art });
    }
  );
};

Articulos.deleteFoto = (id, result) => {
  sql.query("DELETE FROM fotos where idfotos = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

Articulos.updateFotoPrincipal = (id, foto, result) => {
  sql.query(` UPDATE fotos SET principal = ?  WHERE idfotos = ?;`, [foto.principal, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated fotoiculos: ", { id: id, ...foto });
      result(null, { id: id, ...foto });
    }
  );
};

Articulos.getArticuloFotos = (codigo, result) => {
  sql.query(`SELECT * FROM fotos WHERE codigo = ? ORDER BY principal DESC`,[ codigo ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  });
};

Articulos.getArticuloFam = (id) => {
  return new Promise((resolve, reject) => {
    sql.query(`SELECT f.*, fa.familia FROM familias_art f
        LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idarticulo = ? `,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getArticuloCat = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT c.*, ca.categoria FROM categorias_art c
      LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getArticuloSub = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT s.*, sa.subcategoria FROM subcategorias_art s
      LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ?`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getArticuloxFam = (id, result) => {
  sql.query(`SELECT f.*, fa.familia, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM familias_art f
LEFT JOIN arts a ON a.id = f.idarticulo
      LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idfamilias = ?  AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getArticuloxCat = (id, result) => {
  sql.query(`SELECT c.*, ca.categoria, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM categorias_art c
      LEFT JOIN arts a ON a.id = c.idarticulo
    LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idcategorias = ? AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getArticuloxSub = (id, result) => {
  sql.query(`SELECT s.*, sa.subcategoria, a.*, IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto FROM subcategorias_art s
    LEFT JOIN arts a ON a.id = s.idarticulo
    LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idsubcategorias = ? AND a.estatus = 1`,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }
    result(null, res);
  })
};

Articulos.getRelacionadosCat = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT c.*, ca.categoria, a.*,IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto  FROM categorias_art c
      LEFT JOIN arts a ON a.id = c.idarticulo
      LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idcategorias IN(
        SELECT c.idcategorias FROM categorias_art c
        LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?) AND a.estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};

Articulos.getRelacionadosSub = (id) => {
   return new Promise((resolve, reject) => {
    sql.query(`SELECT s.*, sa.subcategoria, a.*,IFNULL((SELECT image_name FROM fotos WHERE codigo = a.codigo ORDER BY principal DESC LIMIT 1),"") AS foto  FROM subcategorias_art s
    LEFT JOIN arts a ON a.id = s.idarticulo
    LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idsubcategorias IN(
    SELECT sa.idsubcategorias FROM subcategorias_art s
      LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ? ) AND a.estatus = 1;`,[ id ], (err, res) => {
      if (err) {
        reject(err);
      }
      resolve(res);
    });
  })
};


module.exports = Articulos;