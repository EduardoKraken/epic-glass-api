const sql = require("../config/db.js");

// constructor
const Banners = bann => {
    this.idbanners   = bann.idbanners;
    this.nombanner   = bann.nombanner;
};

// Agregar un laboratorio
Banners.addBanner = (pcNombre , mobilNombre) => {
  return new Promise((resolve, reject) => {

    sql.query(`INSERT INTO banners(nombanner, banner_mobil)VALUES(?,?);`,
              [pcNombre, mobilNombre], (err, res) => {	
      if (err) {
        console.log("error: ", err);
        reject(err, null);
        return;
      }
      // console.log("Crear Grupo: ", res.insertId);
      console.log("Crear Banners: ", { id: res.insertId, banner_pc: pcNombre, banner_mobil: mobilNombre });
      resolve({ id: res.insertId, banner_pc: pcNombre, banner_mobil: mobilNombre });
    });

  })
};

Banners.bannerPorId = (idbanners) => {
  return new Promise((resolve, reject) => {
    sql.query('SELECT idbanners, nombanner, banner_mobil FROM banners WHERE idbanners = ?',[idbanners], (err, res) => {
      if (err) {
        console.log("error: ", err);
        reject(null, err);
        return;
      }
      resolve(res);
    });
  })
}

// Traer laboratior activos
Banners.getBanners = result => {
  sql.query('SELECT * FROM banners;', (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Banners.eliminar_banner = (idbanners) => {
  return new Promise((resolve, reject) => {
    sql.query("DELETE FROM banners where idbanners = ?;",[idbanners], (err, res) => {
      if (err) {
        console.log("error: ", err);
        reject(null, err);
        return;
      }

      resolve(res);
    });
  })

};


module.exports = Banners;