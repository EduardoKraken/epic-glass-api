const sql = require("../config/db.js");

// constructor
const Clientes = function(clientes) {
  this.idcliente = clientes.idcliente;
  this.nombre    = clientes.nombre;
  this.password  = clientes.password;
  this.telefono  = clientes.telefono;
  this.estatus   = clientes.estatus;
};

// Clientes.login_cliente = (loger,result) =>{
//   sql.query("SELECT * FROM clientes WHERE email = ? AND password = ? or email = ? AND passextra = ?"
//     , [loger.email, loger.password, loger.email, loger.password],(err, res) => {
//     if (err) { 
//       console.log("error: ", err); 
//       result(err, null);
//       return 
//     }
//     console.log('resultado sesion', res[0])
//     result(null, res[0])
//   });
// };

Clientes.login_cliente = ({nombre, password}) =>{
  return new Promise((resolve, reject) => {
    sql.query(`SELECT 
                  idcliente,
                  usuario
                FROM clientes 
              WHERE 
                  usuario = ? 
              AND password = ? 
              AND estatus = 1
              AND es_eliminado = 0`
      , [nombre, password],(err, res) => {
      if (err) { 
        console.log("error: ", err); 
        reject(err);
        return 
      }
      console.log('resultado sesion', res[0])
      resolve(res[0])
    });
  })
};

Clientes.obtenerPasswordSalt = (usuario) => {
  return new Promise((resolve, reject) => {
      sql.query(`SELECT 
                       password_salt AS salt
                  FROM clientes AS c
                  WHERE
                      c.usuario = ?
                  OR c.idcliente = ?`, [usuario, usuario],(err, res) => {
          if (err) {
              console.log('err', err);
              
              reject(err);
              return;
          }
          resolve(res[0])
      });
  })
};

Clientes.validar_existencia_cliente = (u) => {
  return new Promise((resolve, reject) => {
      sql.query(`SELECT 1 
                      FROM clientes 
                  WHERE usuario = ?`,[u.nombre], (err, res) => {
          if (err) {
              reject(err);
              return;
          }
          resolve(res)
      });
  })
};

Clientes.registrar_nuevo_cliente = (c) => {
  return new Promise((resolve,reject)=>{

    sql.query(`INSERT INTO clientes(
                  usuario
                 ,password
                 ,password_salt
              )VALUES(?,?,?)`,
              [
                  c.nombre
                , c.password
                , c.salt
              ], 
      (err, res) => {	
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      resolve(null, { id: res.insertId, ...c });
    });
  })

};

Clientes.guardarTokenCliente = ( idcliente, token ) => {
	return new Promise(( resolve, reject )=>{
        sql.query(`INSERT INTO sesiones_clientes_tokens (idcliente, usu_acces_token)VALUE(?,?)`, 
                    [ idcliente, token],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( { id: res.insertId, idcliente, token});
	  });
	});
};

Clientes.updateCliente = (cli, result) => {
  sql.query(` UPDATE clientes 
              SET nombre   = ?, 
                  apellido = ?, 
                  usuario  = ?,
                  email    = ?
              WHERE idcliente = ?`, 
              [ cli.data.nombre, 
                cli.data.apellido, 
                cli.data.usuario,
                cli.data.email, 
                cli.data.id 
              ],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      result(null, true);
    }
  );
};

Clientes.cambioContraseniaCliente = (p) => {
  return new Promise((resolve, reject) => {
      sql.query(`UPDATE clientes 
                  SET password = ? 
                  WHERE idcliente = ?`, [p.password, p.idcliente], (err, res) => {
          if (err) {
              reject(err);
              return;
          }
          resolve(res)
      });
  })
};






Clientes.getClienteId = (params, result)=>{
	sql.query(`SELECT SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes 
    FROM clientes WHERE idcliente=?`, [params.idweb], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};
Clientes.getClientes = result => {
  sql.query(`SELECT idcliente, nombre, email, telefono,password, estatus FROM clientes`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("clientes: ", res);
    result(null, res);
  });
};


Clientes.OlvideContraCliente = (data,result) =>{
  sql.query(`SELECT idcliente,nombre,email FROM clientes WHERE email = ?`,[data.email], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("email cliente: ", res);
    result(null, res);
  });
};
Clientes.passwordExtraCliente = (user, result)=>{
  sql.query(` UPDATE clientes SET passextra = ? WHERE idcliente = ?`, [ user.codigo, user.id],(err, res) =>  {
      if (err) { result(null, err); return; }
      if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
      result(null, "Cliente actualizado correctamente");
    }
  );
};

module.exports = Clientes;