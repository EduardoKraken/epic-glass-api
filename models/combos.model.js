const sql = require("../config/db.js");

//const constructor
const combos = (clases) => {};

combos.obtener_presentaciones = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT idpresentaciones, presentacion FROM presentaciones WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

combos.obtener_unidades = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT idunidades,unidad FROM unidades WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

combos.obtener_marcas = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT idmarca, marca FROM marcas`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

combos.obtener_olores = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT idolores, olor FROM olores WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}

combos.obtener_productos_bodega_almacen = (idbodega) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT 
                  a.idproductos, 
                  a.cant,
                  p.nomart,
                  p.codigo
                FROM almacen AS a
                JOIN arts AS p ON p.id = a.idproductos
               WHERE idbodegas = ?`,[idbodega],(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = combos;