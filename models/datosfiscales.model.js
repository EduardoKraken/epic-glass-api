const sql = require("../config/db.js");

// constructor
const DatosFiscales = function(fis) {
    this.iddatosfis       = fis.iddatosfis;
    this.idusuariosweb    = fis.idusuariosweb;
    this.calle            = fis.calle;
    this.numext           = fis.numext;
    this.colonia          = fis.colonia;
    this.ciudad           = fis.ciudad;
    this.estado           = fis.estado;
    this.cp               = fis.cp;
    this.telefono         = fis.telefono;
    this.email            = fis.email;
    this.rfc              = fis.rfc;
    this.pais             = fis.pais;
    this.nomcomer         = fis.nomcomer;
    this.numint           = fis.numint;
    this.nomcia           = fis.nomcia;
};

// Agregar un laboratorio
DatosFiscales.addDatosfiscales = (d, result) => {
  sql.query(`INSERT INTO datosfis(idusuariosweb,calle,numext,colonia,ciudad,estado,cp,telefono,email,rfc,pais,nomcomer,numint)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`,[d.idusuariosweb,d.calle,d.numext,d.colonia,d.ciudad,d.estado,d.cp,d.telefono,d.email,d.rfc,d.pais,d.nomcomer,d.numint], (err, res) => { 
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Direcciones: ", { id: res.insertId, ...d });
    result(null, { id: res.insertId, ...d });
  });
};


// Traer laboratorios por id
DatosFiscales.getDatosfiscales =(id, result )=>{
	sql.query(`SELECT * FROM datosfis WHERE iddatosfis = ?`,[ id ],(err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("getDatosfiscales: ", res);
    result(null, res);
	})
};


// Actualizar laboratorios
DatosFiscales.updateDatosfiscales = (d, result) => {
  sql.query(` UPDATE datosfis SET calle = ? ,numext = ? ,colonia = ? ,ciudad = ? ,estado = ? ,cp = ? ,
    telefono = ? ,email = ? ,rfc = ? ,pais = ? ,nomcomer = ?  ,numint = ? WHERE iddatosfis = ?`,
   [d.calle,d.numext,d.colonia,d.ciudad,d.estado,d.cp,d.telefono,d.email,d.rfc,d.pais,d.nomcomer,
                d.numint ,d.iddatosfis],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lab: ", { id: d });
      result(null, { id: d });
    }
  );
};

/*******************************************************************************************************/
/*******************************************************************************************************/
/*******************************************************************************************************/

// Traer laboratorios por id
DatosFiscales.getDatosfiscalesTienda =(result )=>{
  sql.query(`SELECT * FROM cia WHERE id = 1`,(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("getDatosfiscalesTienda: ", res);
    result(null, res);
  })
};


// Actualizar laboratorios
DatosFiscales.updateDatosfiscalesTienda = (d, result) => {
  sql.query(` UPDATE cia SET nomcia = ?, calle = ? ,numext = ? ,colonia = ? ,ciudad = ? ,estado = ? ,cp = ? ,
    telefono = ? ,email = ? ,rfccia = ? , WHERE id = 1`,
   [d.nomcia,,d.calle,d.numext,d.colonia,d.ciudad,d.estado,d.cp,d.telefono,d.email,d.rfccia],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated lab: ", { id: d });
      result(null, { id: d });
    }
  );
};



module.exports = DatosFiscales;