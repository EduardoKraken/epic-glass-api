const sql = require("../config/db.js");

//const constructor
const efectos = (clases) => {};

efectos.addEfectos = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO efectos( efecto, estatus ) VALUES( ?, ? )`, [ u.efecto, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

efectos.updateEfectos = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE efectos SET efecto = ?, estatus = ?, deleted = ?  WHERE idefectos = ?`,[ u.efecto, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

efectos.getEfectoss = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM efectos WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


efectos.getEfectosActivos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM efectos WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = efectos;