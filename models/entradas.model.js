const sql = require("../config/db.js");

// constructor

const Entradas = entradas => {
    this.identradas = entradas.identradas;
    this.nomart = entradas.nomart;
    this.codigo = entradas.codigo;
    this.fechaentr = entradas.fechaentr;
    this.lote = entradas.lote;
    this.cant = entradas.cant;
    this.caducidad = entradas.caducidad;
    this.proveedor = entradas.proveedor;
    this.factura = entradas.factura;
};

// ***************************************************************************************
// *                            FUNCIONES PROBADAS                                       * 
// ***************************************************************************************
Entradas.getEntradasAll = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        e.identradas
                        , a.nomart      AS NOMBRE_PRODUCTO
                        , e.codigo      AS CODIGO_PRODUCTO
                        , e.fechaentr   AS FECHA_ENTRADA
                        , e.cant        AS CANTIDAD
                        , u.nomuser     AS USUARIO_RECIBIO
                        , b.nombre      AS BODEGA
                        , u.idbodegas   AS ID_BODEGA
                    FROM entradas AS e 
                        INNER JOIN usuariosweb AS u ON e.recibio = u.idusuariosweb
                        INNER JOIN arts a ON e.idproductos = a.id
                        INNER JOIN bodegas AS b ON e.idbodegas = b.idbodegas
                     `, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}


Entradas.addEntradas = (art, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO entradas (
                    recibio
                    ,idbodegas 
                    ,idproductos
                    ,codigo
                    ,fechaentr
                    ,cant
                )
                VALUES(?,?,?,?,?,?)`, [
                    art.recibio
                    ,art.idbodegas
                    ,art.idproductos
                    ,art.codigo
                    , art.fechaentr
                    , art.cant 
                ], (err, res) => {
            if (err) {
                console.log("error: ", err);
                reject(err);
                return;
            }
            console.log("Crear nueva entrada: ", { id: res.insertId, ...art });
            resolve({ id: res.insertId, ...art });
        });
    })

};


Entradas.getEntradasFecha = (f) => {
    return new Promise((resolve, reject) => {
        let queryParams = [f.inicio, f.fin, f.idbodegas];
        let conditions = ' WHERE e.fechaentr BETWEEN ? AND ?';

        if (f.idbodegas && f.idbodegas !== 0) {
            conditions += ' AND e.idbodegas = ?';
            queryParams.push(f.idbodegas);
        }

        const query = `SELECT 
                            e.identradas,
                            a.nomart        AS NOMBRE_PRODUCTO,
                            e.codigo        AS CODIGO_PRODUCTO,
                            e.fechaentr     AS FECHA_ENTRADA,
                            e.cant          AS CANTIDAD,
                            u.nomuser       AS USUARIO_RECIBIO,
                            b.nombre        AS BODEGA,
                            u.idbodegas     AS ID_BODEGA
                        FROM entradas AS e 
                            JOIN usuariosweb AS u ON e.recibio = u.idusuariosweb
                            JOIN arts a ON e.idproductos = a.id 
                            JOIN bodegas AS b ON e.idbodegas = b.idbodegas` +
            conditions;

        sql.query(query, queryParams, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Entradas.updateEntrada = (id, ent, result) => {
    sql.query(` UPDATE entradas SET lote = ?, proveedor =?, factura =?, caducidad=?
                WHERE identradas = ?`, [ent.lote, ent.proveedor, ent.factura, ent.caducidad, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated enticulos: ", { id: id, ...ent });
            result(null, { id: id, ...ent });
        }
    );
};

module.exports = Entradas;