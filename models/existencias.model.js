const sql = require("../config/db.js");

// constructor

const Existencias = existencias => {
    this.idexistencias = existencias.idexistencias;
    this.nomart = existencias.nomart;
    this.codigo = existencias.codigo;
    this.lote = existencias.lote;
    this.cant = existencias.cant;
    this.caducidad = existencias.caducidad;
    this.suma = existencias.suma;
    this.lab = existencias.lab;
    this.sal = existencias.sal;
};

Existencias.getExistencias = (idbodegas) => {
    return new Promise((resolve, reject) => {
        // Ejecutar la consulta
        const query = `
            SELECT 
                a.idalmacen     AS ID,
                SUM(a.cant)     AS EXISTENCIAS,
                ar.nomart       AS NOMBRE_PRODUCTO,
                ar.codigo       AS CODIGO_PRODUCTO,
                b.nombre        AS BODEGA,
                a.modificacion  AS ULTIMA_ACTUALIZACION
            FROM almacen AS a
                LEFT JOIN arts      AS ar ON a.idproductos = ar.id
                LEFT JOIN bodegas   AS b  ON a.idbodegas = b.idbodegas
            WHERE (? = 0 OR a.idbodegas = ?)
            GROUP BY a.idalmacen, ar.nomart, ar.codigo, b.nombre,a.modificacion
        `;

        sql.query(query, [idbodegas, idbodegas], (err, results) => {
            if (err) {
                console.log("error: ", err);
                reject(err);
                return;
            }
            console.log("entradas: ", results);
            resolve(results);
        });
    });
};


// Existencias.getExistencias = (idbodegas) => {
//     return new Promise((resolve, reject) => { 
//         sql.query(`SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));`, (err, res) => {
//             if (err) {
//                 console.log("error: ", err);
//                 result(null, err);
//                 return;
//             }
//             sql.query(`SELECT 
//                         a.idalmacen             AS ID
//                         , sum(a.cant)           AS EXISTENCIAS
//                         , ar.nomart             AS NOMBRE_PRODUCTO
//                         , ar.codigo             AS CODIGO_PRODUCTO
//                         , a.modificacion        AS ULTIMA_ACTUALIZACION
//                         FROM almacen AS a
//                     LEFT JOIN arts AS ar ON a.idproductos = ar.id
//                         WHERE 
//                             (? = 0 OR idbodegas = ?)
//                     GROUP BY  ar.id`,[idbodegas], (err, res) => {
//                 if (err) {
//                     console.log("error: ", err);
//                     reject(err);
//                     return;
//                 }
//                 console.log("entradas: ", res);
//                 resolve(res)
//             });
//         });
//     })

// };

Existencias.getExistenciaCodigo = (codigo, result) => {
    sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like'%` + codigo + `%' AND a.cant > 0
        ORDER BY a.caducidad ASC, a.cant DESC ;`, [codigo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};

Existencias.getExistenciaCodigoDes = (codigo, result) => {
    sql.query(`SELECT a.lote, ar.nomart, l.nomlab AS "lab",a.caducidad, SUM(a.cant) AS cant, ar.sal
        FROM almacen a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
        l.idlaboratorios = ar.idlaboratorio 
        WHERE a.codigo = ? AND a.cant > 0  OR  ar.nomart like"%` + codigo + `%" AND a.cant > 0
        GROUP BY a.lote, ar.nomart, l.nomlab, ar.sal,a.caducidad;`, [codigo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};


Existencias.getExistenciaTotal = result => {
    sql.query(`SELECT ar.nomart, a.idalmacen, a.codigo, a.lote, a.caducidad, a.cant, ar.sal, l.nomlab AS "lab"
    FROM almacen  a INNER JOIN arts ar ON a.codigo=ar.codigo INNER JOIN laboratorios l ON 
    l.idlaboratorios = ar.idlaboratorio WHERE a.cant > 0 ORDER BY a.caducidad ASC, a.cant DESC;`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};

module.exports = Existencias;