const sql = require("../config/db.js");

// constructor
const Facturas = function(facturas) {
  this.idfacturas  = facturas.idfacturas;
  this.numfact     = facturas.numfact;
  this.fecha       = facturas.fecha;
  this.fvenc       = facturas.fvenc;
  this.concepto    = facturas.concepto;
  this.importe     = facturas.importe;
  this.fpago       = facturas.fpago;
  this.refer       = facturas.refer;
  this.estatus     = facturas.estatus;
  this.fproxpago   = facturas.fproxpago;
  this.idpagos     = facturas.idpagos;
  this.nomcli      = facturas.nomcli;
  this.pagado      = facturas.pagado;
  this.faltante    = facturas.faltante;
};

Facturas.addFacturas = (c, result) => {
	sql.query(`INSERT INTO  facturas 
      		( numfact, fecha, fvenc, concepto, importe, fpago, refer, estatus, fproxpago, idpagos)VALUES(?,?,?,?,?,?,?,?,?,?)`,
		[c.numfact,c.fecha,c.fvenc,c.concepto,c.importe,c.fpago,c.refer,c.estatus,c.fproxpago,c.idpagos], 
    (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...c });
    result(null, { id: res.insertId, ...c });
	});
};

Facturas.getFacturasId = (idfacturas, result)=>{
	sql.query(`SELECT * FROM facturas WHERE idfacturas = ?`, [idfacturas], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};


Facturas.getFacturas = (id, result) => {
  sql.query(`SELECT 
        f.idfacturas, f.numfact, f.fecha, f.fvenc, f.concepto, f.importe, f.fpago, f.refer, f.estatus, 
        f.fproxpago, f.idpagos, f.importe, c.nomcli, c.idweb, 
        (SELECT sum(importe) FROM pagoxfac WHERE idfacturas = f.idfacturas) AS "pagado",
        f.importe - (SELECT sum(importe) FROM pagoxfac WHERE idfacturas = f.idfacturas) AS "faltante"  
        FROM facturas f INNER JOIN pagos p on f.idpagos = p.idpagos 
        INNER JOIN clientes c ON p.idcliente = c.idweb WHERE f.idpagos = ?`,[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("Pagos: ", res);
    result(null, res);
  });
};


Facturas.updateFacturas = (cli, result) => {
  sql.query(` UPDATE facturas SET
      		numfact =?, fecha =?, fvenc =?, concepto =?, importe =?, fpago =?, refer =?, estatus =?, fproxpago =?, idpagos =?
   			WHERE idfacturas =?`, [cli.numfact, cli.fecha, cli.fvenc, cli.concepto, cli.importe, cli.fpago, cli.refer, cli.estatus, cli.fproxpago, cli.idpagos,cli.idfacturas],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated cli: ", { cli });
      result(null);
    }
  );
};

Facturas.deleteFacturas = (id, result) => {
  sql.query("DELETE FROM facturas WHERE idfacturas = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted facturas with id: ", id);
    result(null, res);
  });
};

module.exports = Facturas;