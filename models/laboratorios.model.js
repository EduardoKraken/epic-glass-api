const sql = require("../config/db.js");

// constructor
const Laboratorio = function(lab) {
    this.idlaboratorios   = lab.idlaboratorios;
    this.nomlab           = lab.nomlab;
    this.estatus          = lab.estatus;
};

/***************************************************************************************************/
/*                                        CATEGORÍA                                                */
/***************************************************************************************************/
Laboratorio.getCategorias = (result)=>{
	sql.query(`SELECT 
                c.idcategorias
                ,c.categoria
                ,c.idfamilias
                ,c.estatus
                ,c.foto
                ,f.familia
               FROM categorias AS c
                   JOIN familias f ON f.idfamilias = c.idfamilias
                WHERE
                  c.es_eliminado = 0`, (err,res)=>{
		if (err) {
      result(null, err);
      return;
    }
    console.log('res', res);
    
    result(null, res);
	})
};

Laboratorio.addCategorias = (n, result) => {
	sql.query(`INSERT INTO categorias(categoria,idfamilias,estatus, foto)VALUES(?,?,?,?)`,
						 [n.categoria,n.idfamilias,n.estatus,n.foto], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
	});
};

Laboratorio.updateCategorias = (id, n, result) => {
  sql.query(` UPDATE categorias SET categoria = ?, idfamilias = ? , estatus = ?, foto = ?
                WHERE idcategorias = ?`, [n.categoria,n.idfamilias, n.estatus, n.foto, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

Laboratorio.eliminarCategoria = (id, result) => {
  sql.query(` UPDATE categorias 
              SET es_eliminado = 1, 
                  estatus = 0
              WHERE idcategorias = ?`, [id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      
      result(null, { id: id});
    }
  );
};


Laboratorio.getCategoriasActivo = result => {
  sql.query(`SELECT * FROM categorias WHERE estatus = 1 AND es_eliminado = 0 ORDER BY categoria`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

/***************************************************************************************************/
/*                                           FAMILIA                                               */
/***************************************************************************************************/
Laboratorio.getFamilias = (result)=>{
  sql.query("SELECT * FROM familias WHERE es_eliminado = 0",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.addFamilias = (n, result) => {
  sql.query(`INSERT INTO familias(familia,estatus)VALUES(?,?)`,
             [n.familia,n.estatus], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.updateFamilias = (id, n, result) => {
  sql.query(` UPDATE familias SET familia = ?, estatus = ?
                WHERE idfamilias = ?`, [n.familia, n.estatus,id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

Laboratorio.eliminarFamilia = (id, result) => {
  sql.query(` UPDATE familias 
              SET es_eliminado = 1, 
                  estatus = 0
              WHERE idfamilias = ?`, [id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      
      result(null, { id: id});
    }
  );
};

Laboratorio.getFamiliasActivo = (result)=>{
  sql.query("SELECT * FROM familias WHERE estatus = 1",(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

/***************************************************************************************************/
/*                                     SUBCATEGORIAS                                               */
/***************************************************************************************************/
Laboratorio.eliminarSubCategoria = (id, result) => {
  sql.query(` UPDATE subcategorias 
              SET es_eliminado = 1, 
                  estatus = 0
              WHERE idsubcategorias = ?`, [id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
      
      result(null, { id: id});
    }
  );
};

Laboratorio.getSubcategorias = (result)=>{
  sql.query(`SELECT 
                s.*
                , c.categoria
                , f.familia 
              FROM subcategorias AS s
                LEFT JOIN categorias AS c ON s.idcatego = c.idcategorias
                LEFT JOIN familias   AS f ON f.idfamilias = c.idfamilias 
              WHERE 
                  s.es_eliminado = 0
              ORDER BY subcategoria
                `, (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.updateSubCatego = (id, n, result) => {
  sql.query(` UPDATE subcategorias SET subcategoria = ?, estatus = ?, idcatego = ?
                WHERE idsubcategorias = ?`, [n.subcategoria, n.estatus,n.idcatego, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      result(null, { id: id, ...n });
    }
  );
};

Laboratorio.addSubcategorias = (n, result) => {
  sql.query(`INSERT INTO subcategorias(subcategoria,idcatego,estatus)VALUES(?,?,?)`,
             [n.subcategoria,n.idcatego,n.estatus], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

// >>>>>>>>> DE AQUI EN ADELANTE ES SECCION NO VERIFICADA >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>>  
// >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>> >>>>>>>>>>>> 
Laboratorio.addCategoriasArt = (n, result) => {
  sql.query(`INSERT INTO categorias_art(idcategorias,idarticulo)VALUES(?,?)`,
             [n.idcategorias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getLabList = result => {
  sql.query(`SELECT * FROM laboratorios ORDER BY nomlab`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  });
};

Laboratorio.getCategoriaPorFamilia = (familia,result)=>{
  sql.query(`SELECT * FROM categorias WHERE idfamilias = ?;`,[familia], (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.getCategoriasArtId = (id,result)=>{
  sql.query(`SELECT c.*, ca.categoria FROM categorias_art c
LEFT JOIN categorias ca ON ca.idcategorias = c.idcategorias WHERE c.idarticulo = ?`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deleteCategoriasArt = (id, result) => {
  sql.query("DELETE FROM categorias_art WHERE idcategorias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

Laboratorio.getCatActivos = (result)=>{
  sql.query("SELECT * FROM laboratorios WHERE estatus = 1 ORDER BY nomlab", (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};



Laboratorio.getSubcategoriasActivo = (result)=>{
  sql.query(`SELECT * FROM subcategorias WHERE estatus = 1`, (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};




Laboratorio.getSubcategoriasPorCatego = (idcatego, result)=>{
  sql.query("SELECT * FROM subcategorias WHERE idcatego = ? ORDER BY subcategoria", [idcatego], (err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.addsubCategoriasArt = (n, result) => {
  sql.query(`INSERT INTO subcategorias_art(idsubcategorias,idarticulo)VALUES(?,?)`,
             [n.idsubcategorias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getsubCategoriasArtId = (id,result)=>{
  sql.query(`SELECT s.*, sa.subcategoria FROM subcategorias_art s
LEFT JOIN subcategorias sa ON sa.idsubcategorias = s.idsubcategorias WHERE s.idarticulo = ?`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deletesubCategoriasArt = (id, result) => {
  sql.query("DELETE FROM subcategorias_art WHERE idsubcategorias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

Laboratorio.addFamiliasArt = (n, result) => {
  sql.query(`INSERT INTO familias_art(idfamilias,idarticulo)VALUES(?,?)`,
             [n.idfamilias,n.idarticulo], (err, res) => {  
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    result(null, { id: res.insertId, ...n });
  });
};

Laboratorio.getFamiliasArtId = (id,result)=>{
  sql.query(`SELECT f.*, fa.familia FROM familias_art f
      LEFT JOIN familias fa ON fa.idfamilias = f.idfamilias WHERE f.idarticulo = ?;`,[id],(err,res)=>{
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    result(null, res);
  })
};

Laboratorio.deleteFamiliasArt = (id, result) => {
  sql.query("DELETE FROM familias_art WHERE idfamilias_art = ?;",[id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log(`deleted ${res.affectedRows} banners`);
    result(null, res);
  });
};

/***************************************************************************************************/
/*                                           TIENDA                                               */
/***************************************************************************************************/

Laboratorio.obtenerFamilias = () => {
  return new Promise((resolve, reject) => {
      sql.query("SELECT idfamilias, familia FROM familias WHERE estatus = 1 AND es_eliminado = 0",(err,res)=>{
      if (err) {
        console.log("error: ", err);
        reject(err);
        return;
      }
      resolve(res)
    })
  })
} 

Laboratorio.obtenerCategorias = () => {
  return new Promise((resolve, reject) => {
      sql.query("SELECT idcategorias, categoria, idfamilias FROM categorias WHERE estatus = 1 AND es_eliminado = 0",(err,res)=>{
      if (err) {
        console.log("error: ", err);
        reject(err);
        return;
      }
      resolve(res)
    })
  })
} 

Laboratorio.obtenerSubcategorias = () => {
  return new Promise((resolve, reject) => {
      sql.query("SELECT idsubcategorias, subcategoria, idcatego FROM subcategorias WHERE estatus = 1 AND es_eliminado = 0",(err,res)=>{
      if (err) {
        console.log("error: ", err);
        reject(err);
        return;
      }
      resolve(res)
    })
  })
} 



// Laboratorio.getMenuTienda = (result)=>{
//   sql.query("SELECT * FROM familias WHERE estatus = 1",(err,familias)=>{
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//       return;
//     }
//     sql.query("SELECT * FROM categorias WHERE estatus = 1",(err,categorias)=>{
//       if (err) {
//         console.log("error: ", err);
//         result(null, err);
//         return;
//       }
//       sql.query("SELECT * FROM subcategorias WHERE estatus = 1",(err,subcategorias)=>{
//         if (err) {
//           console.log("error: ", err);
//           result(null, err);
//           return;
//         }

//         var menu = []

//         for(const i in familias){
//           var famili = {
//             familia    : familias[i].familia,
//             idfamilias : familias[i].idfamilias,
//             categorias : []
//           }
//           menu.push(famili)
//         }

//         for(const i in menu){
//           for(const j in categorias){
//             if(menu[i].idfamilias == categorias[j].idfamilias){
//               menu[i].categorias.push(categorias[j])
//             }
//           }
//         }

//         var menu2 = []
//         for(const i in menu){
//           var famili = {
//             familia    : menu[i].familia,
//             idfamilias : menu[i].idfamilias,
//             categorias : []
//           }
//           for(const j in menu[i].categorias){
//             var cate = {
//               categoria     : menu[i].categorias[j].categoria,
//               idcategorias  : menu[i].categorias[j].idcategorias,
//               subcategorias : []
//             }
//             famili.categorias.push(cate)
              
//           }
//           menu2.push(famili)
//         }

//         for(const i in menu2){
//           for(const j in menu2[i].categorias){
//             for(const k in subcategorias){
//               if(menu2[i].categorias[j].idcategorias == subcategorias[k].idcatego){
//                 menu2[i].categorias[j].subcategorias.push(subcategorias[k])
//               }
//             }
//           }
//         }

//         result(null, menu2);
//       })
//     })
//   })
// };
module.exports = Laboratorio;