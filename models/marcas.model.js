const sql = require("../config/db.js");

//const constructor
const marcas = (clases) => {};

marcas.agregar_marca = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO marcas(marca,estatus ) VALUES( ?, ? )`, [ u.marca, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

marcas.actualiza_marca = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE marcas SET marca = ?, estatus = ?, es_eliminado = ?  WHERE idmarca = ?`,[ u.marca, u.estatus, u.es_eliminado, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }
      resolve( u );
	  });
	});
};

marcas.obtener_marcas_catalogo = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM marcas WHERE es_eliminado = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


marcas.obtener_marcas_activos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM marcas WHERE es_eliminado = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = marcas;