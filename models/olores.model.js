const sql = require("../config/db.js");

//const constructor
const olores = (clases) => {};

olores.agregar_olor = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO olores(olor,estatus ) VALUES( ?, ? )`, [ u.olor, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

olores.actualiza_olor = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE olores SET olor = ?, estatus = ?, deleted = ?  WHERE idolores = ?`,[ u.olor, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }
      resolve( u );
	  });
	});
};

olores.obtener_olores_catalogo = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM olores WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


olores.obtener_olores_activos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM olores WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = olores;