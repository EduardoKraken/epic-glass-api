const sql = require("../config/db.js");

// constructor

const Ordenes = orden => {
    this.iddocum = orden.iddocum;
    this.idusuariosweb = orden.idusuariosweb;
    this.direccion = orden.direccion;
    this.importe = orden.importe;
    this.descuento = orden.descuento;
    this.subtotal = orden.subtotal;
    this.total = orden.total;
    this.iva = orden.iva;
    this.folio = orden.folio;
    this.estatus = orden.estatus;
    this.nota = orden.nota;
    this.divisa = orden.divisa;
    this.hora = orden.hora;
    this.fecha = orden.fecha;
    this.fechapago = orden.fechapago;
    this.refer = orden.refer;
    this.tipodoc = orden.tipodoc;
    this.Movim = orden.Movim;
};


//Modulo : pedidosobtenerPedidosPendiente
Ordenes.getDocumen = (d) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * 
                    FROM docum 
                    WHERE CAST(fecha AS date) BETWEEN ? AND ?`, [d.fecha1, d.fecha2], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};
// Modulos : pedidos
Ordenes.getMovimientos = (idpedidos) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        m.*
                        , a.nomart
                        , a.codigo
                        , c.descripcion
                        , c.largo
                        , c.ancho
                        , c.alto
                        , c.peso
                    FROM movim AS m
                        LEFT JOIN arts              AS a ON a.id = m.id
                        LEFT JOIN caracteristicas   AS c ON c.id_articulo = m.id
                    WHERE iddocum = ? `, [idpedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

// *******************

Ordenes.addOrden = (o, result) => {
    sql.query(`INSERT INTO docum SET idusuariosweb= ? ,direccion= ? ,importe= ? ,descuento= ? ,subtotal= ? ,total= ? ,iva= ? ,folio= ? ,estatus= ? ,nota= ? ,divisa= ? ,hora= ? ,fecha= ? ,fechapago= ? ,refer= ? ,tipodoc = ? `, [o.idusuariosweb, o.direccion, o.importe, o.descuento, o.subtotal, o.total, o.iva, o.folio, o.estatus, o.nota, o.divisa, o.hora, o.fecha, o.fechapago, o.refer, o.tipodoc], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        } else {
            var id = res.insertId
            var folio = 'SURTI-' + id
            sql.query(`UPDATE docum SET folio = ? WHERE iddocum = ?`, [folio, res.insertId], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                } else {
                    o.movim.forEach((element, index) => {
                        sql.query(`INSERT INTO movim SET codigo= ? ,foto= ? ,descrip= ? ,cantidad= ? ,precio= ? ,descuento= ? ,iva= ? ,neto = ?, iddocum= ?`, [element.codigo, element.foto, element.descrip, element.cantidad, element.precio1, element.descuento, element.iva, element.total, id], (err, res) => {
                            if (err) {
                                console.log("error: ", err);
                                result(err, null);
                                return;
                            }
                        });
                        if (index == (o.movim.length - 1)) {
                            console.log("Crear Pedido: ", { id: res.insertId, ...o });
                            result(null, { id: res.insertId, ...o });
                        }
                    })
                }
            });

        }


    });
};



// Falta actualizar el folio y agregar el Movim
Ordenes.getDocumenCli = (id) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM docum WHERE idcliente = ${id}`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};




Ordenes.getDirecciones = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM pago_direcciones;`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.getDireccionesFac = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM pago_facturacion;`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.getClientesOrden = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM clientes;`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.getPagosMercaddoPago = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM pagos_mercadopago;`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.getOrdenes = (id, result) => {
    sql.query(`SELECT * FROM docum WHERE idcliente = ?;`, [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};


Ordenes.getOrdenesCliente = (result) => {
    sql.query(`SELECT * FROM docum ;`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};

Ordenes.getOrdenId = (id, result) => {
    sql.query(`SELECT * FROM docum WHERE iddocum = ?;`, [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};

Ordenes.getMovim = (id, result) => {
    sql.query(`SELECT * FROM movim WHERE iddocum = ?;`, [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("entradas: ", res);
        result(null, res);
    });
};

// Ordenes.getEntradasFecha = (fecha, result) => {
//   sql.query(`SELECT e.identradas, a.nomart, e.codigo, e.fechaentr, e.lote, e.cant, e.caducidad, e.proveedor, e.factura 
//         FROM  entradas e INNER JOIN arts a ON e.codigo = a.codigo WHERE fechaentr BETWEEN ? AND ?`,
//     [fecha.inicio,fecha.fin], (err, res) => {  
//     	console.log(sql)
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//       return;
//     }
//     console.log("entradas: ", res);
//     result(null, res);
//   });
// };

Ordenes.updateOrden = (id, ent, result) => {
    sql.query(` UPDATE docum SET estatus = ?, nota = ? WHERE iddocum = ?`, [ent.estatus, ent.nota, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated enticulos: ", { id: id, ...ent });
            result(null, { id: id, ...ent });
        }
    );
};

Ordenes.updateOrdenEnvio = (id, ent, result) => {
    sql.query(` UPDATE docum SET trackingnumber = ? WHERE iddocum = ?`, [ent.trackingnumber, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated enticulos: ", { id: id, ...ent });
            result(null, { id: id, ...ent });
        }
    );
};

Ordenes.getRastreoCliente = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT * FROM rastreo;`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.asignarMaquiladorPedido = (p) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO seguimiento_pedidos (
                  idpedidos
                , idbodegas 
                , idmaquilador
                , asignacion_maquilador 
                , obs_asignacion_maquilador
              ) VALUES(?,?,?, NOW(),?)`, [p.idpedidos, p.idbodegas, p.idmaquilador, p.obs_asignacion_maquilador], (err, res) => {
            console.log('err insert', err);
            if (err) {
                reject(err);
                return;
            }
            console.log('res insert', res);

            resolve(res)
        });
    })
};

Ordenes.actualizarPedido = (estatus, idpedidos) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE docum 
                SET estatus = ?
              WHERE iddocum = ?`, [estatus, idpedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.cobroEnvioRepartidor = (idpedidos, envio) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE docum 
                SET envio_repartidor = ? 
              WHERE iddocum = ?`, [envio, idpedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.obtenerPedidosxMaquilador = (p) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT s.*, u.nomuser AS nombre_repartidor  
                    FROM  seguimiento_pedidos AS s
                        JOIN docum AS d  ON s.idpedidos = d.iddocum
                        LEFT JOIN usuariosweb AS u ON s.idrepartidor = u.idusuariosweb
                    WHERE 
                        s.idbodegas = ? 
                    AND s.idmaquilador = ?
                    AND d.estatus = 3`, [p.idbodegas, p.idmaquilador], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.obtenerProductosxPedido = (iddocum) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
										m.idmovim 
										,m.iddocum  AS idpedidos
										,m.id 	    AS idproducto
										,m.foto
										,m.cantidad
										,a.nomart
										,a.codigo
									FROM movim AS m
										JOIN arts AS a ON m.id = a.id 
									WHERE
										m.iddocum = ?`, [iddocum], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.actualizarProcesoPedido = (p, updateFields, obsUpdate) => {
    return new Promise((resolve, reject) => {
        let fechaActual = new Date();
        sql.query(`UPDATE seguimiento_pedidos 
                SET proceso = ?,
                    ${updateFields} = ?,
                    ${obsUpdate} = ?
              WHERE idseguimiento_pedidos = ?`, [p.proceso, fechaActual, p.obs, p.idseguimiento_pedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.asignarRepartidorPedido = (p) => {
    return new Promise((resolve, reject) => {
        let fechaActual = new Date();
        sql.query(`UPDATE seguimiento_pedidos 
                    SET idrepartidor = ?,
                        asignacion_repartidor = ?,
                        obs_asignacion_repartidor = ?,
                        proceso=?
                    WHERE idseguimiento_pedidos= ?`, [
            p.idrepartidor,
            fechaActual,
            p.obs_asignacion_repartidor,
            p.proceso,
            p.idseguimiento_pedidos
        ], (err, res) => {
            console.log('err insert', err);
            if (err) {
                reject(err);
                return;
            }
            console.log('res insert', res);

            resolve(res)
        });
    })
};
// Consulta orientada a repartidores
Ordenes.obtenerPedidosPendiente = (p) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        s.idseguimiento_pedidos
                        , s.idpedidos
                        , s.proceso
                        , s.asignacion_repartidor
                        , s.retenido
                        , d.total
                        , concat(p.calle,' ',p.numero,' ', p.colonia, ', ', p.municipio,', ',p.estado,', ', p.cp) AS direccion
                    FROM  seguimiento_pedidos AS s
                        LEFT JOIN docum         AS d ON d.iddocum = s.idpedidos 
                        LEFT JOIN direcciones   AS p ON d.iddirecion = p.iddireccion
                    WHERE 
                        s.idrepartidor = ?
                    AND s.proceso <> 6
                    AND d.estatus = 3
                    AND CAST(s.asignacion_repartidor AS DATE) = ?`, [p.idrepartidor, p.fecha], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.guardarRegistroImagen = (id, image_name) => {
    return new Promise((resolve, reject) => {
        sql.query(`INSERT INTO evidencias_entregas (
                  idseguimiento_pedidos
                , image_name 
              ) VALUES(?,?)`, [id, image_name], (err, res) => {
            console.log('err img', err);
            if (err) {
                reject(err);
                return;
            }
            console.log('res img', res);

            resolve(res)
        });
    })
};

Ordenes.historialPedido = (idpedidos) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        s.*
                        ,b.nombre  AS bodega
                        ,m.nomuser AS maquilador
                        ,r.nomuser AS repartidor
                    FROM  seguimiento_pedidos AS s
                        LEFT JOIN docum 		 AS d ON s.idpedidos = d.iddocum
                        LEFT JOIN bodegas 	 AS b ON s.idbodegas = b.idbodegas
                        LEFT JOIN usuariosweb AS m ON s.idmaquilador = m.idusuariosweb
                        LEFT JOIN usuariosweb AS r ON s.idrepartidor = r.idusuariosweb
                    WHERE 
                        s.idpedidos = ?`, [idpedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Ordenes.evidenciasEntregas = (idseguimiento_pedidos) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        e.image_name 
                    FROM evidencias_entregas AS e
                    WHERE 
                        e.idseguimiento_pedidos = ?`, [idseguimiento_pedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Ordenes.retenerPedido = (p) => {
    return new Promise((resolve, reject) => {
        let fechaActual = new Date();
        sql.query(`UPDATE seguimiento_pedidos 
                SET proceso      = 7,
                    retenido     = NOW(),
                    obs_retenido = ?,
                    es_retenido  = 1
              WHERE idpedidos = ?`, [p.obs, p.idpedidos], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}






module.exports = Ordenes;