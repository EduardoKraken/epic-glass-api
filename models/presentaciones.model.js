const sql = require("../config/db.js");

//const constructor
const presentaciones = (clases) => {};

presentaciones.agregar_presentacion = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO presentaciones( presentacion, estatus ) VALUES( ?, ? )`, [ u.presentacion, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

presentaciones.actualiza_presentacion = ( u, id ) => {
  console.log('u',u);
  console.log('id',id);

  
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE presentaciones SET presentacion = ?, estatus = ?, deleted = ?  WHERE idpresentaciones = ?`,[ u.presentacion, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

presentaciones.obtener_presentacion_catalogo = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM presentaciones WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


presentaciones.obtener_presentaciones_activos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM presentaciones WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = presentaciones;