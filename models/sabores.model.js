const sql = require("../config/db.js");

//const constructor
const sabores = (clases) => {};

sabores.addSabores = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO sabores( sabor, estatus ) VALUES( ?, ? )`, [ u.sabor, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

sabores.updateSabores = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE sabores SET sabor = ?, estatus = ?, deleted = ?  WHERE idsabores = ?`,[ u.sabor, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

sabores.getSabores = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM sabores WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


sabores.getSaboresActivos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM sabores WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = sabores;