const sql = require("../config/db.js");

// constructor

const Salidas = entradas => {
    this.idsalidas = entradas.idsalidas;
    this.codigo = entradas.codigo;
    this.caducidad = entradas.caducidad;
    this.cant = entradas.cant;
    this.lote = entradas.lote;
    this.fecha = entradas.fecha;
    this.recibido = entradas.recibido;
    this.entrego = entradas.entrego;
    this.nomart = entradas.nomart;
    this.nomuser = entradas.nomuser;
};


Salidas.addSalidas = (art, result) => {
    return new Promise((resolve, reject) => {

        sql.query(`INSERT INTO salidas (
                    idbodegas
                    ,idproductos
                    ,codigo
                    ,cant
                    ,fecha
                    ,entrego
                    ,recibio
                )
                VALUES(?,?,?,?,?,?,?)`, [
                    art.idbodegas, 
                    art.idproductos,
                    art.codigo, 
                    art.cant, 
                    art.fecha, 
                    art.entrego,
                    art.recibio 
            ], (err, res) => {
            if (err) {
                console.log("error: ", err);
                reject(err, null);
                return;
            }
            console.log("Crear salida: ", { id: res.insertId, ...art });
            resolve({ id: res.insertId, ...art });
        });
    })

};


Salidas.getSalidasAll = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        s.idsalidas
                        , s.codigo      AS CODIGO_PRODUCTO
                        , s.cant        AS CANTIDAD
                        , s.fecha       AS FECHA_SALIDA
                        , a.nomart      AS NOMBRE_PRODUCTO
                        , s.cant        AS CANTIDAD
                        , s.idbodegas   AS ID_BODEGA
                        , b.nombre      AS BODEGA
                    FROM salidas AS s 
                        JOIN arts         AS a ON s.idproductos = a.id 
                        JOIN bodegas      as b ON s.idbodegas = b.idbodegas
                    WHERE s.cant > 0`, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

Salidas.getSalidasFecha = (f) => {
    return new Promise((resolve, reject) => {
        let queryParams = [f.inicio, f.fin, f.idbodegas];
        let conditions = ' WHERE s.fecha BETWEEN ? AND ?';

        if (f.idbodegas && f.idbodegas !== 0) {
            conditions += ' AND s.idbodegas = ?';
            queryParams.push(f.idbodegas);
        }

        // conditions += ' ORDER BY s.fecha ASC'

        const query = `SELECT 
                            s.idsalidas
                            , s.codigo      AS CODIGO_PRODUCTO
                            , s.cant        AS CANTIDAD
                            , s.fecha       AS FECHA_SALIDA
                            , a.nomart      AS NOMBRE_PRODUCTO
                            , s.cant        AS CANTIDAD
                            , s.idbodegas   AS ID_BODEGA
                            , b.nombre      AS BODEGA
                        FROM salidas s 
                            JOIN arts         AS a ON s.idproductos = a.id 
                            JOIN bodegas      as b ON s.idbodegas = b.idbodegas
                    ` +
            conditions;

        sql.query(query, queryParams, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
}

module.exports = Salidas;