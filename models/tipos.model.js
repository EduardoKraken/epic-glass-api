const sql = require("../config/db.js");

//const constructor
const tipos = (clases) => {};

tipos.addTipos = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO tipos( tipo, estatus ) VALUES( ?, ? )`, [ u.tipo, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

tipos.updateTipos = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE tipos SET tipo = ?, estatus = ?, deleted = ?  WHERE idtipos = ?`,[ u.tipo, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }

      resolve( u );
	  });
	});
};

tipos.getTipos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM tipos WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


tipos.getTiposActivos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM tipos WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = tipos;