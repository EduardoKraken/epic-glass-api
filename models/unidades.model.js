const sql = require("../config/db.js");

//const constructor
const unidades = (clases) => {};

unidades.agregar_unidad = ( u ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`INSERT INTO unidades(unidad,estatus ) VALUES( ?, ? )`, [ u.unidad, u.estatus ],(err, res) => {
      if(err){
        return reject({ message: err.sqlMessage });
      }
      resolve({ id: res.insertId, ...u })
    })
  })
}

unidades.actualiza_unidad = ( u, id ) => {
	return new Promise(( resolve, reject )=>{
	  sql.query(`UPDATE unidades SET unidad = ?, estatus = ?, deleted = ?  WHERE idunidades = ?`,[ u.unidad, u.estatus, u.deleted, id ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }
      resolve( u );
	  });
	});
};

unidades.obtener_unidades_catalogo = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM unidades WHERE deleted = 0;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


unidades.obtener_unidades_activos = ( ) => {
  return new Promise((resolve,reject)=>{
    sql.query(`SELECT * FROM unidades WHERE deleted = 0 AND estatus = 1;`,(err, res) => {
      if( err ){ return reject({ message: err.sqlMessage })}
      resolve(res)
    })
  })
}


module.exports = unidades;