const sql = require("../config/db.js");

// constructor
const Users = function(user) {
    this.email = user.email;
    this.nomuser = user.nomuser;
    this.estatus = user.estatus;
    this.nomper = user.nomper;
    this.password = user.password;
    this.nivel = user.nivel;
    this.estatus = user.estatus;
};
// VARIABLES PARA QUERYS

Users.login = ({ email, password }) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        idusuariosweb 
                        ,nomuser
                        ,email
                        ,telefono
                        ,celular 
                        ,nivel 
                        ,estatus 
                        ,nomper 
                        ,idniveles
                        ,idbodegas
                    FROM usuariosweb 
                    WHERE 
                        email = ? 
                    AND password = ? 
                    OR 
                        nomuser = ? 
                    AND password = ?
                    AND es_eliminado = 0
                    AND estatus = 2`, [email, password, email, password], (err, res) => {
            if (err) {
                console.log("error iniciar sesión: ", err);
                reject(err, null);
                return
            }
            resolve(res[0])
        });
    })
};

Users.obtenerPasswordSalt = (idusuariosweb) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                         password_salt AS salt
                    FROM usuariosweb AS u
                    WHERE
                        u.idusuariosweb = ? OR u.email = ?`, [idusuariosweb, idusuariosweb],(err, res) => {
            if (err) {
                console.log('err', err);
                
                reject(err);
                return;
            }
            resolve(res[0])
        });
    })
};

// *******************************************************************************
// *                    FUNCIONES PROBADAS Y USADAS                              *
// *******************************************************************************
Users.obtener_usuarios_catalogo = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                         u.idusuariosweb
                        ,u.nomuser      AS nombre 
                        ,u.nomper       AS usuario
                        ,u.email        AS correo
                        ,u.telefono     AS telefono
                        ,u.celular      AS celular 
                        ,n.idnivel      AS idnivel
                        ,n.nombre       AS nivel
                        ,u.estatus      AS idestatus
                        ,CASE 
                            WHEN 2 THEN 'ACTIVO'    
                            WHEN 3 THEN 'BAJA'      
                            WHEN 0 THEN 'INACTIVO'  
                        END             AS estatus 
                        ,u.idbodegas    AS idbodegas
                        ,b.nombre       AS bodega
                    FROM usuariosweb AS u
                        JOIN niveles AS n ON n.idnivel = u.idniveles 
                        LEFT JOIN bodegas AS b ON b.idbodegas = u.idbodegas
                    WHERE
                        u.es_eliminado = 0 
                   `, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Users.validar_existencia_usuario = (u) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 1 
                        FROM usuariosweb 
                    WHERE email = ? AND idniveles = ?`,[u.correo, u.nivel], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Users.agregar_nuevo_usuario_catalogo = (u) => {
    return new Promise((resolve,reject)=>{
      sql.query(`INSERT INTO usuariosweb(
                     nomuser
                    , email
                    , telefono
                    , celular
                    , password
                    , nomper
                    , idniveles
                    , idbodegas 
                    , estatus
                    , password_salt
                )VALUES( ?,?,?,?,?,?,?,?,?,?)`, 
                [     u.nombre
                    , u.correo
                    , u.telefono
                    , u.celular
                    , u.password
                    , u.usuario
                    , u.nivel
                    , u.bodega
                    , 2 
                    , u.salt
                ],(err, res) => {
        if(err){
          return reject({ message: err.sqlMessage });
        }
        resolve({ id: res.insertId, ...u })
      })
    })
};

Users.validar_modificacion_usuario = (id, u) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 1 
                        FROM usuariosweb 
                    WHERE 
                        email = ? 
                    AND idniveles = ?
                    AND idusuariosweb <> ?`,[u.correo, u.nivel, id ], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Users.actualizar_usuario_catalogo = ( id, u ) => {
	return new Promise(( resolve, reject )=>{
        sql.query(`UPDATE usuariosweb 
                    SET   nomuser   = ?
                        , email     = ?
                        , telefono  = ?
                        , celular   = ?
                        , nomper    = ? 
                        , idniveles = ?
                        , idbodegas = ?
                        , estatus   = ?
                    WHERE idusuariosweb = ?`, 
                    [ u.nombre
                        , u.correo
                        , u.telefono
                        , u.celular
                        , u.usuario
                        , u.nivel
                        , u.bodega
                        , u.estatus
                        , id
                    ],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }
      resolve( u );
	  });
	});
};

Users.cambioContrasenia = (p) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE usuariosweb 
                    SET password = ? 
                    WHERE idusuariosweb = ?`, [p.password, p.idusuario], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Users.eliminar_usuario_catalogo = ( idusuariosweb ) => {
	return new Promise(( resolve, reject )=>{
        sql.query(`UPDATE usuariosweb 
                    SET   es_eliminado = 1
                        , estatus = 3
                    WHERE idusuariosweb = ?`, 
                    [ idusuariosweb],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      if (res.affectedRows == 0) {
        return  reject({ kind: "not_found" });
      }
      resolve(res);
	  });
	});
};

Users.guardarToken = ( idusuariosweb, token ) => {
	return new Promise(( resolve, reject )=>{
        sql.query(`INSERT INTO sesiones_tokens (idusuariosweb, usu_acces_token)VALUE(?,?)`, 
                    [ idusuariosweb, token],
	    (err, res) => {
      if (err) {
        return reject({ message: err.sqlMessage });
      }
      resolve( { id: res.insertId, idusuariosweb, token});
	  });
	});
};



// *******************************************************************************
// *                    FUNCIONES NO PROBADAS                                    *
// *******************************************************************************

Users.deleteSchoolsxUser = (id, result) => {
    sql.query("DELETE FROM usuariosweb WHERE id_user = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} user`);
        result(null, res);
    });
};

Users.createSchoolsxUser = (id, school, result) => {
    sql.query("INSERT INTO user_schools(id_user, id_school)VALUE(?,?)", [id, school], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        // console.log("created user: ", { id: res.insertId, ...newUser });
        // result(null, res);
    });
};
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

Users.OlvideContra = (data, result) => {
    sql.query(`SELECT idusuariosweb, email,nomuser FROM usuariosweb WHERE email = ?`, [data.email], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("email: ", res);
        result(null, res);
    });
};

Users.passwordExtra = (user, result) => {
    sql.query(` UPDATE usuariosweb SET passextra = ? WHERE idusuariosweb = ?`, [user.codigo, user.id], (err, res) => {
        if (err) { result(null, err); return; }
        if (res.affectedRows == 0) { result({ kind: "not_found" }, null); return; }
        result(null, "Usuario actualizado correctamente");
    });
};
// --- SIN USAR---->
Users.create = (newUser, result) => {
    console.log('crear un usuario', newUser);
    sql.query("INSERT INTO usuariosweb SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created user: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};

Users.findById = (userid, result) => {
    sql.query(`SELECT 
                    idusuariosweb
                    ,nomuser
                    ,email
                    ,nivel
                    ,estatus
                    ,nomper
                    ,idniveles
                    ,idbodegas
                FROM usuariosweb 
            WHERE idusuariosweb = ${userid}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            result(null, res[0]);
            return;
        }
        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Users.getxEmail = (data, result) => {
    if (data.nomuser == '') {
        sql.query(`SELECT * FROM usuariosweb WHERE email = ?`, [data.email], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found user: ", res[0]);
                result(null, res[0]);
                return;
            }
            // not found Customer with the id
            result({ kind: "not_found" }, null);
        });
    } else {
        sql.query(`SELECT * FROM usuariosweb WHERE email = ? OR nomuser = ?`, [data.email, data.nomuser], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found user: ", res[0]);
                result(null, res[0]);
                return;
            }
            // not found Customer with the id
            result({ kind: "not_found" }, null);
        });
    }

};

Users.getAll = result => {
    sql.query("SELECT * FROM usuariosweb", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("users: ", res);
        result(null, res);
    });
};

Users.updateById = (id, user, result) => {
    sql.query("UPDATE usuariosweb SET email = ?, name = ?, active = ? WHERE id = ?", [user.email, user.name, user.active, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found user with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated user: ", { id: id, ...user });
            result(null, { id: id, ...user });
        }
    );
};

Users.remove = (id, result) => {
    sql.query("DELETE FROM usuariosweb WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted user with id: ", id);
        result(null, res);
    });
};

Users.removeAll = result => {
    sql.query("DELETE FROM usuariosweb", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} user`);
        result(null, res);
    });
};

Users.activarUsuario = (idusuario, result) => {
    sql.query(`UPDATE usuariosweb SET estatus = 2 WHERE idusuariosweb = ?`, [idusuario], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        } // SI OCURRE UN ERROR LO RETORNO
        if (res.affectedRows == 0) { result({ kind: "No encontrado" }, null); return; } // SI NO ENCUENTRO EL ID

        console.log("Se actualizo el usuario: ", { idusuario: idusuario });
        result(null, { idusuario: idusuario });
    });
};

Users.obtenerTipoUsuario = (data, result) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                   u.idusuariosweb AS id 
                  ,u.nomuser       AS nombre
                  ,b.nombre        AS bodega
              FROM usuariosweb AS u 
                JOIN bodegas AS b ON u.idbodegas = b.idbodegas
              WHERE 
                u.idbodegas = ? AND u.idniveles = ?`, [data.idbodegas, data.idniveles], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

Users.obtenerNiveles = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        idnivel AS id,
                        nombre  AS nombre,
                        codigo 
                    FROM niveles 
                    WHERE activo = 1
                   `, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

Users.usuariosAdmin = () => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                        idusuariosweb
                        ,nomuser   AS nombre  
                    FROM usuariosweb 
                    WHERE 
                        estatus = 2 
                    AND idniveles = 1
                   `, (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

module.exports = Users;