module.exports = app => {
  const aromas = require('../controllers/aromas.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.post("/aromas.add"        , aromas.addAromas);   // BUSCAR 
  app.put("/aromas.update/:id"  , aromas.updateAromas);
  app.get("/aromas.list"        , aromas.getAromas);   // BUSCAR 
  app.get("/aromas.activo"      , aromas.getAromasActivos);   // BUSCAR 

};