module.exports = app => {
    const articulos = require('../controllers/articulos.controllers') // --> ADDED THIS

    app.get("/obtener.articulos.random", articulos.obtener_articulos_random);   // BUSCAR 
    app.get("/obtener.articulos"       , articulos.obtener_articulos);   // BUSCAR 
    app.post("/fotos.add"                , articulos.addFoto);


    // Apis generales
    app.get("/articulos.id/:id"          , articulos.getArticuloId);    //  Edición articulos
    app.put("/articulos.update/:id"      , articulos.updateArticulos);  //  Edición articulos
    
    app.get("/articulos.list"            , articulos.findAll);   // BUSCAR 
    app.post("/articulos.add"            , articulos.addArticulos);
    app.get("/articulos.codigo/:codigo"  , articulos.getArticuloCodigo);
    app.get("/articulos.fotos/:codigo"   , articulos.fotosxArt);
    app.get("/articulos.activos"         , articulos.getArticulosActivo);


    // Novedades
    app.put("/articulos.novedades/:id", articulos.updateNovedades);
    app.get("/obtener.novedades",   articulos.obtener_novedades);
    app.get("/obtener.destacados",  articulos.obtener_destacados);
    app.get("/obtener.juguetes",    articulos.obtener_juguetes);

    // Foto
    app.delete("/admin/foto.art.delete/:id",   articulos.deleteFoto);

    // Tienda
    app.get("/tienda/articulos.novedades.get",       articulos.getNovedades);
    app.get("/tienda/articulos.destacados.get",      articulos.getDestacados);
    app.get("/tienda/articulos.promociones.get",     articulos.getPromociones);
    app.get("/tienda/articulos.laboratorio.get/:id", articulos.getArtxLab);
    app.get("/tienda/articulos.codigo/:codigo",      articulos.getArticuloTienda);
    app.get("/tienda/articulos.articulo/:id",        articulos.getArticuloId);
    app.get("/tienda/articulos/fotos/:codigo",       articulos.getArticuloFotos)
    app.get("/tienda/articulos/fam/:id",             articulos.getArticuloFam)
    app.get("/tienda/articulos/cat/:id",             articulos.getArticuloCat)
    app.get("/tienda/articulos/sub/:id",             articulos.getArticuloSub)

    // Foto principal
    app.put("/update.foto.principal/:id", articulos.updateFotoPrincipal);

    // apis para traer asrticulos por familias, categorias y subcategorias
    app.get("/tienda/articulos.fam/:id",             articulos.getArticuloxFam)
    app.get("/tienda/articulos.cat/:id",             articulos.getArticuloxCat)
    app.get("/tienda/articulos.sub/:id",             articulos.getArticuloxSub)

    app.get("/tienda/articulos.relacionados/:id",    articulos.getRelacionados)


  };