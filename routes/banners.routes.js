module.exports = app => {
  const banners = require('../controllers/banners.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/banner.add",     	banners.addBanner);
  app.get("/banner.list",     	banners.getBanners);
  app.post("/banner.eliminar", 	banners.eliminar_banner);
};