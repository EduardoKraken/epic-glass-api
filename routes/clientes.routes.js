module.exports = app => {
  const clientes = require('../controllers/clientes.controllers') // --> ADDED THIS
  const authUser = require('../middlewares/authUser');


  app.post("/session.cliente"  , clientes.session_cliente);
  app.post("/clientes.add"     , clientes.registrar_nuevo_cliente);   // CREAR UN NUEVO USUARIO
  app.post("/clientes.update"  , clientes.updateCliente); // PERFIL EN TIENDA
  app.post("/cambio.contrasenia.cliente", clientes.cambioContraseniaCliente); // PERFIL CLIENTES - CAMBIO CONTRASEÑA


  app.get("/clientes.id"       , clientes.getClienteId);   
  app.post("/clientes.list"    , clientes.getClientes);   
  app.post("/olvida.contra.cliente" , clientes.OlvideContraCliente);
  app.post("/password.extra.cliente", clientes.passwordExtraCliente);

};