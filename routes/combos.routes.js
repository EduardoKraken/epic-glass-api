module.exports = app => {
  const combos = require('../controllers/combos.controllers')
  // Rutas para el administrador
  app.get("/presentaciones.list"  , combos.obtener_presentaciones);    
  app.get("/unidades.list"        , combos.obtener_unidades);    
  app.get("/marcas.list"          , combos.obtener_marcas);    
  app.get("/olores.list"          , combos.obtener_olores);    

  app.get("/productos.bodega.almacen/:idbodega", combos.obtener_productos_bodega_almacen)

};