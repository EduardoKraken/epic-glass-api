module.exports = app => {
    const configuracion = require('../controllers/configuracion.controllers') // --> ADDED THIS

    // movil
    app.get("/config.list", configuracion.getConfiguracion); // BUSCAR
    app.put("/config.update", configuracion.updateConfig);

    app.get("/obtener.costo.envio", configuracion.costoEnvio);
    app.post("/actualiza.costo.envio", configuracion.actualizaCostoEnvio);
};