module.exports = app => {
  const deseos = require('../controllers/deseos.controllers') // --> ADDED THIS

  app.get("/obtener.lista.deseos/:id_cliente", deseos.obtener_lista_deseos);
  app.post("/anadir.lista.deseos"            , deseos.anadir_lista_deseos);
  app.delete("/eliminar.producto.lista.deseos/:idlista", 	deseos.eliminar_producto_lista_deseos);
};