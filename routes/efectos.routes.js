module.exports = app => {
  const efectos = require('../controllers/efectos.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.post("/efectos.add"		     , efectos.addEfectos);   
  app.put("/efectos.update/:id"  , efectos.updateEfectos);
  app.get("/efectos.list"        , efectos.getEfectoss);    
  app.get("/efectos.activo"      , efectos.getEfectosActivos);    

};