module.exports = app => {
  const existencias = require('../controllers/existencias.controllers') // --> ADDED THIS

  // movil
  app.get("/existencias.list/:idbodegas",     existencias.getExistencias); 
  app.get("/existencias.codigo/:codigo", 		  existencias.getExistenciaCodigo);  
  app.get("/existencias.total",          		  existencias.getExistencias);   
  app.get("/existencias.codigo.des/:codigo", 	existencias.getExistenciaCodigoDes)
};