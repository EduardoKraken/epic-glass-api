// routes/index.js
const express = require('express');
const router = express.Router();

// Importar todas las rutas
require('./users.routes')(router);
require('./articulos.routes')(router);
require('./laboratorios.routes')(router);
require('./clientes.routes')(router);
require('./correos.routes')(router);
require('./entradas.routes')(router);
require('./almacen.routes')(router);
require('./salidas.routes')(router);
require('./existencias.routes')(router);
require('./documentos.routes')(router);
require('./banners.routes')(router);
require('./configuracion.routes')(router);
require('./paypal.routes')(router);
require('./direcciones.routes')(router);
require('./datosfiscales.routes')(router);
require('./ordenes.routes')(router);
require('./pagos.routes')(router);
require('./depositos.routes')(router);
require('./facturas.routes')(router);
require('./ciudades.routes')(router);
require('./deseos.routes')(router);
require('./envios.routes')(router);
require('./mercadopago.routes')(router);
require('./cupones.routes')(router);
require('./tipos.routes')(router);
require('./efectos.routes')(router);
require('./aromas.routes')(router);
require('./sabores.routes')(router);
require('./combos.routes')(router);
require('./presentaciones.routes')(router);
require('./marcas.routes')(router);
require('./olores.routes')(router);
require('./unidades.routes')(router);

module.exports = router;
