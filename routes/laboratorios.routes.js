module.exports = app => {
  const laboratorios = require('../controllers/laboratorios.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.get("/categorias.all"            	 , laboratorios.getCategorias);       // Modulo de Categorias  
  app.post("/categorias.foto"            , laboratorios.addFotoCategoria);    // Modulo de Categorías.
  app.post("/categorias.add"		         , laboratorios.addCategorias);       // Modulo de Categorias
  app.put("/categorias.update/:id"       , laboratorios.updateCategorias);    // Modulo de Categorias
  app.put("/categorias.eliminar/:id"     , laboratorios.eliminarCategoria);   // Modulo de Categorias
  app.get("/categorias.activo"           , laboratorios.getCategoriasActivo); // Modulo de Subcategorias


  app.get("/categorias.familia/:id"           , laboratorios.getCategoriaPorFamilia);   
  app.get("/categorias.art.id/:id"            , laboratorios.getCategoriasArtId);
  app.post("/categorias.art.add"              , laboratorios.addCategoriasArt);
  app.delete("/categorias.art.delete/:id"     , laboratorios.deleteCategoriasArt); 

  // Rutas para las subcategorias
  app.get("/subcategorias.all"          , laboratorios.getSubcategorias);     // Modulo de SubCtegorias   
  app.put("/subcategorias.update/:id"   , laboratorios.updateSubCatego);      // Modulo de SubCtegorias
  app.post("/subcategorias.add"         , laboratorios.addSubcategorias);     // Modulo de SubCtegorias   
  app.put("/subcategorias.eliminar/:id" , laboratorios.eliminarSubCategoria); // Modulo de Categorias


  app.get("/subcategorias.categoria/:idcatego"    , laboratorios.getSubcategoriasPorCatego);    
  app.get("/subcategorias.activo"                 , laboratorios.getSubcategoriasActivo);    
  app.get("/subcategorias.art.id/:id"             , laboratorios.getsubCategoriasArtId);
  app.post("/subcategorias.art.add"               , laboratorios.addsubCategoriasArt);
  app.delete("/subcategorias.art.delete/:id"      , laboratorios.deletesubCategoriasArt);

  /***************************************************************************************************/
  /*                                           FAMILIA                                               */
  /***************************************************************************************************/
  app.get("/familias.all"                   , laboratorios.getFamilias);       // Modulo de Familia
  app.put("/familias.eliminar/:id"          , laboratorios.eliminarFamilia);   // Modulo de Familia
  app.post("/familias.add"                  , laboratorios.addFamilias);       // Modulo de Familia
  app.put("/familias.update/:id"            , laboratorios.updateFamilias);    // Modulo de Familia
  app.get("/familias.activo"                , laboratorios.getFamiliasActivo);  // Modulo de Categorias

  app.get("/familias.art.id/:id"            , laboratorios.getFamiliasArtId);
  app.post("/familias.art.add"              , laboratorios.addFamiliasArt);
  app.delete("/familias.art.delete/:id"     , laboratorios.deleteFamiliasArt);


  /***************************************************************************************************/
  /*                                           TIENDA                                                */
  /***************************************************************************************************/

  app.get("/menu.tienda"            , laboratorios.getMenuTienda); 


};