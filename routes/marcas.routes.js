module.exports = app => {
  const marcas = require('../controllers/marcas.controllers') // --> ADDED THIS
  // Rutas para el administrador
  app.post("/marcas.add"		     , marcas.agregar_marca);  
  app.put("/marcas.update/:id"   , marcas.actualiza_marca);
  app.get("/marcas.catalogo"     , marcas.obtener_marcas_catalogo);  
  app.get("/marcas.activo"       , marcas.obtener_marcas_activos);  
};