module.exports = app => {
    const mercadopago = require('../controllers/mercadopago.controllers') // --> ADDED THIS

    app.post("/mercadopago.add"                         , mercadopago.mercadopagoAdd);   // BUSCAR 
    app.get("/mercadopago.success"                      , mercadopago.mercadopagoSuccess);   // BUSCAR 
    app.get("/mercadopago.validarpago/:collection_id"   , mercadopago.validarExistePago);   // BUSCAR 
    app.post("/mercadopago.add.pedido"                  , mercadopago.addPagoMercadopago);   // BUSCAR 
    app.put("/mercadopago.update/:idPago"               , mercadopago.updateMercadoPago);   // BUSCAR 
    app.post("/mercadopago.eliminar.pedido"             , mercadopago.deleteMercadoPedido);   // BUSCAR 
    
    app.get("/obtener.pedidos.cliente/:idcliente"      , mercadopago.obtener_pedidos_cliente);   // BUSCAR 
    app.get("/obtener.productos.movim/:iddocum"        , mercadopago.obtener_productos_movim);   // BUSCAR 

    app.get("/obtener.rastreo/:idpago"         , mercadopago.getRastreo);   // BUSCAR 
    app.post("/agregar.rastreo"                , mercadopago.addRastreo);   // BUSCAR 

  };