module.exports = app => {
  const olores = require('../controllers/olores.controllers') // --> ADDED THIS
  // Rutas para el administrador
  app.post("/olores.add"		     , olores.agregar_olor);  
  app.put("/olores.update/:id"   , olores.actualiza_olor);
  app.get("/olores.catalogo"     , olores.obtener_olores_catalogo);  
  app.get("/olores.activo"       , olores.obtener_olores_activos);  
};