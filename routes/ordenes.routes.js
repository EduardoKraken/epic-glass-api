module.exports = app => {
    const ordenes = require('../controllers/ordenes.controllers') // --> ADDED THIS
    const authUser = require('../middlewares/authUser');

    // Agregar orden
    // app.post("/ordenes.list", authUser,ordenes.getOrdenes); // Modulo: Pedidos
    // console.log('req.usu', req.usuario);
    
    // Estructura de petición para manejo de errores.
    app.post("/ordenes.list", authUser, (req, res) => {
        ordenes.getOrdenes(req, res)
            .then(result => res.send(result))
            .catch(error => res.send(error))
    });

    app.post("/ordenes.add", ordenes.addOrden); // CREAR UN NUEVO GRUPO

    // // todas las ordenes
    app.get("/ordenes.list/:idcliente", ordenes.getOrdenesCliente);
    app.get("/ordenes.id/:id", ordenes.getOrdenId);
    // // por tipo de documento
    app.put("/ordenes.update/:id", ordenes.updateOrden);
    // todas las ordenes
    app.get("/ordenes.usuario/:id", ordenes.getOrdenesUsuario);
    // // Ordenes por usuario 
    app.put("/ordenes.addenvio/:id", ordenes.updateOrdenEnvio);

    // SEGUIMIENTO A PEDIDOS
    app.post("/asignar.maquilador.pedido", ordenes.asignarMaquiladorPedido);
    app.post("/asignar.repartidor.pedido", ordenes.asignarRepartidorPedido);
    app.post("/obtener.maquilaciones.usuario", ordenes.obtenerMaquilacionesUsuario);
    app.post('/actualizar.proceso.pedido', ordenes.actualizarProcesoPedido);
    app.post("/obtener.pedidos.pendientes", ordenes.PedidosPendientes);
    app.post("/finalizar.entrega", ordenes.finalizarEntrega);
    app.post("/obtener.historial.pedido", ordenes.historialPedido);
    app.post("/retener.pedido", ordenes.retenerPedido);

};