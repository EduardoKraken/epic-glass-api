module.exports = app => {
  const presentaciones = require('../controllers/presentaciones.controllers') // --> ADDED THIS
  // Rutas para el administrador
  app.post("/presentaciones.add"		     , presentaciones.agregar_presentacion);  
  app.put("/presentaciones.update/:id"   , presentaciones.actualiza_presentacion);
  app.get("/presentaciones.catalogo"     , presentaciones.obtener_presentacion_catalogo);  
  app.get("/presentaciones.activo"       , presentaciones.obtener_presentaciones_activos);  
};