module.exports = app => {
  const sabores = require('../controllers/sabores.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.post("/sabores.add"		     , sabores.addSabores);   // BUSCAR 
  app.put("/sabores.update/:id"  , sabores.updateSabores);
  app.get("/sabores.list"         , sabores.getSabores);   // BUSCAR 
  app.get("/sabores.activo"      , sabores.getSaboresActivos);   // BUSCAR 

};