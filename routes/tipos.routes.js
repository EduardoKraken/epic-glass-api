module.exports = app => {
  const tipos = require('../controllers/tipos.controllers') // --> ADDED THIS

  // Rutas para el administrador
  app.post("/tipos.add"        , tipos.addTipos);   
  app.put("/tipos.update/:id"  , tipos.updateTipos);
  app.get("/tipos.list"        , tipos.getTipos);   
  app.get("/tipos.activo"      , tipos.getTiposActivos);   

};