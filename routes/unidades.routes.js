module.exports = app => {
  const unidades = require('../controllers/unidades.controllers') // --> ADDED THIS
  // Rutas para el administrador
  app.post("/unidades.add"		     , unidades.agregar_unidad);  
  app.put("/unidades.update/:id"   , unidades.actualiza_unidad);
  app.get("/unidades.catalogo"     , unidades.obtener_unidades_catalogo);  
  app.get("/unidades.activo"       , unidades.obtener_unidades_activos);  
};