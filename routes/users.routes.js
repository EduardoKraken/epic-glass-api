module.exports = app => {
    const users = require('../controllers/users.controllers') // --> ADDED THIS
    const authUser = require('../middlewares/authUser');
    
    // FUNCIONES UTILIZADAS
    app.post("/sessions", users.session);
    app.get("/obtener.usuarios.catalogo", users.obtener_usuarios_catalogo);            // CATALOGO DE USUARIOS
    app.post("/agregar.nuevo.usuario.catalogo", users.agregar_nuevo_usuario_catalogo); // CATALOGO DE USUARIOS
    app.put("/updateuser/:idusuariosweb", users.actualizar_usuario_catalogo);          // CATALOGO DE USUARIOS
    app.post("/cambio.contrasenia", users.cambioContrasenia);                          // CATALOGO DE USUARIOS - CAMBIO CONTRASEÑA
    app.delete("/eliminar.usuario.catalogo/:idusuariosweb", users.eliminar_usuario_catalogo);


    // *FUNCIONES AUN SIN VERIGICAR ***************************************************************************
    // ********************************************************************************************************
    // Create a new Users
    app.post("/user.add", users.create);                 
    // Retrieve all users
    app.get("/users", users.findAll);
    // Retrieve a single Customer with customerId
    app.get("/users/:userId", users.findOne);
    // Traer un usuario por email
    app.post("/user.email", users.getxEmail);
    // Update a Customer with customerId
    app.put("/users/:userId", users.update);
    // Activar usuario
    app.put("/usuario.activar/:idusuario", users.activarUsuario);

    app.post("/olvida.contra", users.OlvideContra);
    app.post("/password.extra", users.passwordExtra);

    // FUERA DE LOGIN
    app.post("/obtener.tipo.usuario", users.obtenerTipoUsuario);
    app.get("/obtener.niveles", users.obtenerNiveles)
    app.get("/obtener.usuarios.admin", users.usuariosAdmin);

};