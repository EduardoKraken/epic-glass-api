const NodeRSA = require("node-rsa");
// const key = new NodeRSA({ b: 512 });
const key = new NodeRSA({ b: 2048 }); 

const keypair = {
    private: key.exportKey(),
    public: key.exportKey("public")
};

console.log('KEYPAIR', keypair);

// Imprimir las claves generadas
// console.log('Clave privada:', keypair.private);
// console.log('Clave pública:', keypair.public);
