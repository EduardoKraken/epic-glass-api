
// server.js
require('dotenv').config(); // Cargar variables de entorno
const http = require('http');
const app = require('./app'); // Importar la aplicación desde app.js

const PORT = process.env.PORT || 3000;

// Iniciar el servidor HTTP
const server = http.createServer(app);

server.listen(PORT, () => {
        console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
        console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
        console.log("|**************************************************************| ");
        console.log(`|************ Servidor Corriendo en el Puerto ${PORT} ************| `);
});
    
